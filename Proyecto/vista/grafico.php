<?php
	require_once("../lib/compartido.php");
	include("../lib/fusioncharts.php");
    validarSesion();
?>
<!DOCTYPE html>

<html>
	<head>
		<title>Gráficos</title>
		<script src="https://static.fusioncharts.com/code/latest/fusioncharts.js"></script>
		<script src="https://static.fusioncharts.com/code/latest/fusioncharts.charts.js"></script>
		<script src="https://static.fusioncharts.com/code/latest/themes/fusioncharts.theme.fint.js"></script>
		
		<?php
			head();
		?>
		
	</head>

	<body>
		<?php
			// Barra superior
			navbar();
			$conn = conectarBD();
            
            $sql = "select ciudad.nombre as ciudades, (select count(*) as cant_sucursales from se_ubica ubicacion where ubicacion.id_ciudad = ciudad.id_ciudad) from ciudad";
            $stmt = $conn->prepare($sql);
    
            $res = ejecutarSQL($stmt);  
            $resultado = $res["salida_exitosa"];           

	        if ($resultado) {

	            $arrData = array(
		            "chart" => array(
			        "caption"=> "Cantidad de sucursales por ciudad", #TITULO DEL GRAFICO
		 	        "captionFontSize"=> "28",
			        "captionFontColor"=> "#000000", #Titulo
			        "captionPadding"=> "20",

			        "baseFont"=> "Merriweather, sans-serif",
			        "baseFontColor"=> "#000000", # Letras eje x
			        "outCnvBaseColor"=> "#000000",
			        "baseFontSize"=> "15",
			        "outCnvBaseFontSize"=> "15",

			        "divLineColor"=> "#ABA39D", # Rejilla del fondo
			        "divLineAlpha"=> "22",
			        "numDivLines"=> "10",	#EJE Y, NUMERO DE DIVICIÓN DE LINEAS

			        "yAxisMinValue"=> "0",
			        "yAxisMaxValue"=> "10",	 #ESCALA DEL EJE Y VA DE 0 A 10

			        "toolTipBorderColor"=> "#000000",
			        "toolTipBgColor"=> "#ffffff",
			        "toolTipPadding"=> "13",
			        "toolTipAlpha"=> "50",
			        "toolTipBorderThickness"=> "2",
			        "toolTipBorderAlpha"=> "30",
			        "toolTipColor"=> "#4D394B",
			        "plotToolText"=> "<div style='text-align: center; line-height: 1.5;'>\$label<br>\$value Sucursales <div>",


			        "theme"=> "fint",
			        "paletteColors"=> "#737373", # Color barras
			        "showBorder"=> "0",
			        "bgColor"=> "#FAF6F3",
			        "canvasBgColor"=> "#ffffff",
			        "bgAlpha"=> "100",
			        "showValues"=> "0",
			        "formatNumberScale"=> "0",
			        "plotSpacePercent"=> "33",
			        "showcanvasborder"=> "0",
			        "showPlotBorder"=> "0"
		        )
	        );
             
	        $arrData["data"] = array();

            foreach($res["datos"] as $row){
		        array_push($arrData["data"], array(
			        "label" => $row["ciudades"],
			        "value" => $row["cant_sucursales"]
			        )
		        );
	        }   
            
	        $jsonEncodedData = json_encode($arrData);

	        $postgresChart = new FusionCharts("column2d", "topMedalChart" , '100%', '450', "postgres-chart", "json", $jsonEncodedData);

	        $postgresChart->render();

        }
			
		?>

        <br><br><br>
	</body>
	<center><div id="postgres-chart"></div></center>

	<br><br><br><br><br><br><br><br><br><br><br><br>  
    <footer class="page-footer bg-dark" >
	    <br><br><br><br>    
    </footer> 
</html>

