<?php
	require_once("../lib/compartido.php");
?>

<!DOCTYPE html> 
<html>
    
    <head>
        <title> Gatos | Patitas </title>
		<?php
			head();
		?>
        
    </head>

     
    <body style="background-color:#f2f2f2;">

		<!-- banner --> 
		<div class="container p-5 my-5" align="center">
			<img align="center" src="./imagenes/y.jpg" width="100%">
		</div>
		
		<!-- Card para productos --> 
		<div id="lista_servicio">
			<div class="container">	
				<div class="titulo" align="center"><h4>Gatos</h4>
					<h5>En esta sección encontrarás alimentos, accesorios y medicamentos para tu mascota</h5>
				</div>
				<br><br>
		</div>	
		
        <div class="container">
		    <div class="tabla"> 
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="tabla_gatos" class="display table compact nowrap"></table>
                            </div>
                        </div> 
                    </div> 
                </div>
            </div>  
        </div>  
		
		<br><br><br><br>

		<footer class="page-footer bg-dark" >
			<div class="container" align="center">
				<br><br>
				<FONT COLOR="white">
                    <h5 class="white-text">
                        Si tienes alguna duda, consulta o sugerencia no dudes en llamarnos al 222266339 o escríbenos al mail contacto@patitas.cl y nos contactaremos contigo a la brevedad.
                        <br> 
                        <a class="grey-text text-lighten-3" href="../vista/sucursal.html">Conócenos</a>
                    </h5>
                </FONT>
			</div> 
            <br><br>
			<script src="../controlador/gatos.js"></script>
        </footer> 

    </body> 

    <?php
		pushbar();
	?>
		
</html> 
