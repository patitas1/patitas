<?php
	require_once("../lib/compartido.php");
?>

<!DOCTYPE html> 
<html>

	<head>
		
		<title> Patitas Tienda de Mascotas </title>
		<?php
			head();
		?>
		
    </head>

    <body style="background-color:#f2f2f2;">

		<?php
			navbar();
		?>

		<!-- columnas --> 
		<div class="container p-5 my-5" align="center">
			<img align="center" src="./imagenes/patitas6.png" width="30%">
		</div>

		<!-- Card para productos --> 
		<div class="container" align="center">
			<div class="card mb-6" style="max-width: 500%;">
				<div class="row g-0">
					<div class="col-md-6">
						<img src="./imagenes/6.jpg" alt="..." class="img-fluid"/>
					</div>

					<div class="col-md-6">
						<div class="card-body">
							<br><br><br>
							<h3 class="card-title" align="left">Comida roedores</h3>
							<h3 class="card-title" align="left">$1.200</h3>
							<div class="login-form"><br>
								<form>
									<div class="form-group" align="left">
										<label>Comida Champion Mini Pets para hamster y gerbos</label><br>
										<label>500 g</label><br><br>
										<label> <small>Categoría: Exóticos</small></label>
									</div><br><br>

									<div class="container" align="left">
										<button >-</button>
										<input type="text" style="text-align: center; width: 30px;" value="1">
										<button >+</button>
									</div>
								</form>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

		<br><br><br>

		<!-- card de pago --> 
		<div class="container" align="center">
			<div class="card mb-6" style="max-width: 500%;">
				<div class="row g-0">
					<div class="col-md-6">
						<div class="card-body">
							<!-- <div class="container" align="center"> -->
							<div class="login-form" style="max-width: 80%;">
								<h3 class="card-title" align="left">Datos personales</h3>
								<form action="/action_page.php">
									<div class="form-group" align="left">
										<div class="form-group" align="left">
											<label for="uname">Rut:</label>
											<input type="text" class="form-control" id="uname" placeholder="Ingrese rut" name="uname" >
										</div>
									</form>
			 
									<form action="/action_page.php" class="was-validated">
										<div class="form-group" align="left">
											<label for="uname">Usuario:</label>
											<input type="text" class="form-control" id="uname" placeholder="Ingrese usuario" name="uname" required>
											<div class="valid-feedback">Válido.</div>
											<div class="invalid-feedback">Por favor rellene este campo.</div>
										</div>
										<div class="form-group" align="left">
											<label for="uname">Correo:</label>
											<input type="text" class="form-control" id="uname" placeholder="Ingrese correo" name="uname" required>
											<div class="valid-feedback">Válido.</div>
											<div class="invalid-feedback">Por favor rellene este campo.</div>
										</div>
									</form> 
									
									<h3 class="card-title" align="left">Método de entrega</h3>
									<form action="/action_page.php">
										<div class="form-group form-check"  align="left">
											<label class="form-check-label">
												<input class="form-check-input" type="checkbox" name="remember" required> Tienda
											</label>
										</div>
										<div class="form-group form-check"  align="left">
											<label class="form-check-label">
												<input class="form-check-input" type="checkbox" name="remember" required> Delivery
											</label>
										</div>
									</form>
						
									<form action="/action_page.php" class="was-validated">
										 <div class="form-group" align="left">
											 <label for="uname">Dirección:</label>
											 <input type="text" class="form-control" id="uname" placeholder="Ingrese dirección" name="uname" required>
											 <div class="valid-feedback">Válido.</div>
											 <div class="invalid-feedback">Por favor rellene este campo.</div>
										</div>
									</form>
					
									<form action="/action_page.php" class="was-validated">
										<div class="form-group" align="left">
											<label for="uname">Ciudad:</label>
											<input type="text" class="form-control" id="uname" placeholder="Ingrese ciudad" name="uname" required>
											<div class="valid-feedback">Válido.</div>
											<div class="invalid-feedback">Por favor rellene este campo.</div>
										</div>
									</form>
								</div>
							</div> <!--   </div> -->
						</div> <!-- cierra body --> 
					</div> <!-- cierra primera columna -->   
					<div class="col-md-6">
						<div class="card-body">
							<h3 class="card-title" align="left">Método de Pago</h3>
							
							<form action="/action_page.php" class="was-validated">
								<div class="form-group" align="left">
									<label for="uname">Número de tarjeta:</label>
									<input type="text" class="form-control" id="uname" placeholder="Ingrese número de tarjeta" name="uname" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>
								
								<div class="form-group" align="left">
									<label for="uname">CVV:</label>
									<input type="text" class="form-control" id="uname" placeholder="Ingrese CVV de su tarjeta" name="uname" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>
							</form> 
							
							<br>
							
							<h3 class="card-title" align="left">Resumen de compra</h3>
							<div class="container" align="left">
								<label>Cantidad de productos: 1</label> <br>
								<label><b>Total:</b> $1.200</label>
							</div>
							
							<br><br><br><br><br><br><br>
							
							<button type="submit" class="btn btn-secondary">Finalizar compra</button>
						</div>
					</div>
				</div>  <!-- cierra row --> 
			</div> <!-- cierra card --> 
		</div> <!-- cierra container --> 

		<br><br><br><br><br>
    
        <!-- Footer-->        
		<footer class="page-footer bg-dark" >
			<div class="container" align="center">
				<br><br>
				<FONT COLOR="white">
                    <h5 class="white-text">
                        Si tienes alguna duda, consulta o sugerencia no dudes en llamarnos al 222266339 o escríbenos al mail contacto@patitas.cl y nos contactaremos contigo a la brevedad.
                        <br> 
                        <a class="grey-text text-lighten-3" href="../vista/sucursal.html">Conócenos</a>
                    </h5>
                </FONT>
			</div> 
            <br><br>
        </footer> 
        
    </body>
    
    <?php
		pushbar();
	?>
</html> 
