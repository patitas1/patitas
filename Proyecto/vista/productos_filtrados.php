<?php
	require_once("../lib/compartido.php");
?>

<!DOCTYPE html> 
<html>
	
    <head>
		<?php
            $nombre = $_REQUEST['nombre'];
		    echo "<title>" . $nombre . " | Patitas </title>";
			head();
		?>
		
    </head>
    
    <body style="background-color:#f2f2f2;">

		<?php
			navbar();

            $banner = $_REQUEST['i'];

            echo "<div class='container p-5 my-5' align='center'>";
			    echo "<img align='center' src='./imagenes/".$banner. "'" . " width='100%'>";
				echo "<input type='hidden' id='btn_cuadrado' name='btn_cuadrado' value=" . $banner . " >";
		    echo "</div>";

		    echo "<div class='container'>";
			    echo "<div class='titulo' align='center'> <h4>". $nombre ."</h4>";
				echo "<input type='hidden' id='btn_circulo' name='btn_circulo' value=" . $nombre . " >";
				    echo "<h5>En esta sección encontrarás alimentos, accesorios y medicamentos para tu mascota</h5>";
			    echo "</div>";
		        echo "<br><br>";
		    echo "</div>";
		?>

        <div class="container">
		    <div class="tabla"> 
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="tabla_filtrado" class="display table compact nowrap"></table>
                            </div>
                        </div> 
                    </div> 
                </div>
            </div>  
        </div>  
		
		<br><br><br><br>

		<footer class="page-footer bg-dark" >
			<div class="container" align="center">
				<br><br>
				<FONT COLOR="white">
                    <h5 class="white-text">
                        Si tienes alguna duda, consulta o sugerencia no dudes en llamarnos al 222266339 o escríbenos al mail contacto@patitas.cl y nos contactaremos contigo a la brevedad.
                        <br> 
                        <a class="grey-text text-lighten-3" href="../vista/sucursal.html">Conócenos</a>
                    </h5>
                </FONT>
			</div> 
            <br><br>
			<script src="../controlador/patitas.js"></script>
			<script src="../controlador/productos_filtrados.js"></script>
        </footer> 

    </body> 

    <?php
		pushbar();
	?>
</html> 
