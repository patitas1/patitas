<?php
	require_once("../lib/compartido.php");
?>

<!DOCTYPE html> 
<html>
    
    <head>

		<title> Ingresar | Patitas Tienda de Mascotas </title>
		<?php
			head()
		?>
		
    </head>

     
    <body style="background-color:#f2f2f2;">
		
		<?php
			navbar();
		?>

		<!-- columnas --> 
 
		<div class="container p-5 my-5" align="center">
			<img align="center" src="./imagenes/patitas6.png" width="30%">
		</div>

		<!-- Card para productos --> 
		<div id="ingreso" class="container" align="center">
			<div class="card mb-6" style="max-width: 500%;">
				<div class="row g-0">
					<div class="col-md-6">
						<img src="./imagenes/fonfo.jpg" alt="..." class="img-fluid"/>
					</div>
					<div class="col-md-6">
						<div class="card-body"> <br><br><br>
							<h5 class="card-title">Iniciar Sesión</h5>
							<div class="login-form"> <br>
								<form id="formulario_ingreso" name="formulario_ingreso" action="" method="POST">
									<div class="form-group" align="left">
										<label>Correo Electrónico</label>
										<input id="correo" type="text" class="form-control" name="correo" placeholder="Correo Electrónico" required>
									</div>
									<div class="form-group" align="left">
										<label>Contraseña</label>
										<input id="contraseña" type="password" class="form-control" name="contraseña" placeholder="Contraseña" required>
									</div>
									<br><br>
									<button id="btn_ingreso" type="submit" class="btn btn-secondary">Iniciar Sesión</button>
									<button name="btn_registro" id="btn_registro" type="submit" class="btn btn-black">Registrarse</button>
								</form>
							</div>
							<p class="card-text">
								<small id="recuperar" class="text-muted">Recuperar contraseña <a class="grey-text text-lighten-3" href="#!">aquí</a></</small>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		 <!-- modals registro-->
		<div class="modal" id="modal-registro">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- header modal -->
					<div class="modal-header">
						<h4 class="modal-title"><span id="titulo-modal-registro">Crear</span></h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				
					<!-- body modal-->
					<div class="modal-body">
				
						<div class="row">
							<form id="form-rut" role="form" method="post" enctype="multipart/form-data">
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<label for="rut">Rut:</label>
										<input type="text" class="form-control" id="rut" placeholder="Ingrese rut" name="rut" >
								</div>
							    <div class="clearfix"></div>
							</form>
			 
							<form id="form-registro" role="form" method="post" enctype="multipart/form-data" class="was-validated">
					 
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label for="usuario">Usuario:</label>
									<input type="text" class="form-control" id="usuario" placeholder="Ingrese nombre de usuario" name="usuario" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label for="email">Correo:</label>
									<input type="text" class="form-control" id="email" placeholder="Ingrese correo" name="email" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label for="contrasena">Contraseña:</label>
									<input type="password" class="form-control" id="contrasena" placeholder="Ingrese una contraseña" name="contrasena" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label for="confirmar">Confirmar contraseña:</label>
									<input type="password" class="form-control" id="confirmar" placeholder="Ingrese nuevamente la contraseña" name="confirmar" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label for="direccion">Dirección:</label>
									<input type="text" class="form-control" id="direccion" placeholder="Ingrese dirección" name="direccion" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>
								
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<form action="/action_page.php">
										<div class="form-group">
											<label for="ciudad">Ciudad:</label>
											<select class="form-control" id="ciudad" name="ciudad">
                                                <option value="" selected="seleccionado"></option>
											</select>
										</div>
									</form>
								</div>
							        <div class="clearfix"></div>
							</form>
						</div>
					</div>
				
					<!-- footer modal -->
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" id="btn-aceptar-registro">Aceptar</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>            
					</div>
				</div>
			</div>
		</div>
		
        <!-- modals recuperar-->
		<div class="modal" id="modal-recuperar">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- header modal -->
					<div class="modal-header">
						<h4 class="modal-title"><span id="titulo-modal-recuperar">Crear</span></h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
				
					<!-- body modal-->
					<div class="modal-body">
				
						<div class="row">
							<form id="form-recuperar" role="form" method="post" enctype="multipart/form-data" class="was-validated">
					 
								<div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<label for="correo_recuperar">Correo:</label>
									<input type="text" class="form-control" id="correo_recuperar" placeholder="Ingrese su correo" name="correo_recuperar" required>
									<div class="valid-feedback">Válido.</div>
									<div class="invalid-feedback">Por favor rellene este campo.</div>
								</div>
					  
								<div class="clearfix"></div>
							</form>
						</div>
					</div>
				
					<!-- footer modal -->
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" id="btn-aceptar-recuperar">Aceptar</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>            
					</div>
				</div>
			</div>
		</div>
		<br><br><br><br><br>
	
		<!-- pie de pagina --> 
		<footer class="page-footer bg-dark" >
			<div class="container" align="center"><br><br>
				<FONT COLOR="white"><h5 class="white-text">Si tienes alguna duda, consulta o sugerencia no dudes en llamarnos al 222266339 o escríbenos al mail contacto@patitas.cl y nos contactaremos contigo a la brevedad. <br> <a class="grey-text text-lighten-3" href="#!">Conócenos</a></h5></FONT>
			</div><br><br>
		</footer> 
		
		<?php
		pushbar();
		?>
		
		
		<script src="../controlador/registro.js"></script>
		<script src="../controlador/ingresar.js"></script>
		<script src="../controlador/recuperar_contra.js"></script>
	</body> 	
</html> 
