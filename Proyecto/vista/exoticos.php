<!DOCTYPE html> 
<html>
    <head>

	<meta charset="utf-8">
	<link rel="icon" href="./imagenes/huella.png">
        <title> Exóticos | Patitas </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	    
<!-- carga archivo css -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
<!-- carga archivos javascript -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
    </head>

     
    <body style="background-color:#f2f2f2;">

<!-- barra de arriba --> 
<nav class="navbar navbar-dark bg-dark" >
<a class="navbar-brand" >
	<!-- casa -->  
<svg xmlns="http://www.w3.org/2000/svg" width="55" height="40" fill="currentColor" class="bi bi-house" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
  <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
</svg> 

</a>
<!-- lado izquierdo -->
  <form class="form-inline">
	  <!--    <input class="form-control mr-sm-1" type="search" placeholder="Buscar" aria-label="Search"> -->
	  <div class="input-group mb-0.5">
          <input type="text" class="form-control" placeholder="Buscar" aria-label="Buscar" aria-describedby="basic-addon2">
          <div class="input-group-append">
          <button class="btn btn-outline-secondary" type="button"> 
<!-- lupa -->
          <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="white" class="bi bi-search" viewBox="0 0 16 16">
          <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
          </svg>

</button> 
          </div>
          </div> 

<!-- carrito --> 
<svg xmlns="http://www.w3.org/2000/svg" width="55" height="35" fill="white" class="bi bi-cart4" viewBox="0 0 16 16">
  <path d="M0 2.5A.5.5 0 0 1 .5 2H2a.5.5 0 0 1 .485.379L2.89 4H14.5a.5.5 0 0 1 .485.621l-1.5 6A.5.5 0 0 1 13 11H4a.5.5 0 0 1-.485-.379L1.61 3H.5a.5.5 0 0 1-.5-.5zM3.14 5l.5 2H5V5H3.14zM6 5v2h2V5H6zm3 0v2h2V5H9zm3 0v2h1.36l.5-2H12zm1.11 3H12v2h.61l.5-2zM11 8H9v2h2V8zM8 8H6v2h2V8zM5 8H3.89l.5 2H5V8zm0 5a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0zm9-1a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm-2 1a2 2 0 1 1 4 0 2 2 0 0 1-4 0z"/>
</svg>


<!-- usuario -->
    <svg xmlns="http://www.w3.org/2000/svg" width="55" height="35" fill="white" class="bi bi-person-circle" viewBox="0 0 16 16">
    <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
    <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
    </svg>
  </form>
</nav>


<!-- columnas --> 

<div class="container p-5 my-5" align="center">
<img align="center" src="./imagenes/t.jpg" width="100%">
</div>



<!-- Card para productos --> 

<div id="lista_servicio">
<div class="container">
	
<div class="titulo" align="center"><h4>Exóticos</h4>
<h5>En esta sección encontrarás alimentos, accesorios y medicamentos para tu mascota</h5>
</div>
<br>
<br>
<div class="card-deck">
			<div class="card" align="center">
			<div class="text-center">
				<img src="./imagenes/18.jpg" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
			</div>
			<div class="card-block">
		<h4 class="card-title" height="90" width="50">Anticloro pecera</h4>
		<p class="card-text">Anticloro para peces 45 ml.</p>
		<p class="card-text">$990</p>
		<h3 " "</h3>
			</div>
			<div class="card-footer text-center">
				<a href="#" class="btn btn-dark">Comprar</a>
			</div>
		</div>
		<div class="card" align="center">
			<div class="text-center">
				<img src="./imagenes/8.jpg" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
			</div>
			<div class="card-block">
				<h4 class="card-title">Tarro de comida</h4>
		<p class="card-text">Comida para tortuga 250 ml</p>
		<p class="card-text">$7.990</p>
		<h3 " "</h3>
			</div>
			<div class="card-footer text-center">
				<a href="#" class="btn btn-dark">Comprar</a>
			</div>
		</div>
		<div class="card" align="center">
			<div class="text-center">
				<img src="./imagenes/20.jpg" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
			</div>
			<div class="card-block">
				<h4 class="card-title">Jugue de mascota</h4>
				<p class="card-text">Rueda de plástico para hámster 12 Cm</p>
				<p class="card-text">$23.300</p>
		<h3 " "</h3>
			</div>
			<div class="card-footer text-center">
				<a href="#" class="btn btn-dark">Comprar</a>
			</div>
		</div>
	 </div>
	</div>
		
		
	<br>
			<div class="container">
				<div class="card-deck">
							<div class="card" align="center">
				<div class="text-center">
					<img src="./imagenes/21.jpg" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
				</div>
				<div class="card-block">
			<h4 class="card-title" height="90" width="50">Jaula de mascota</h4>
			<p class="card-text">Jaula de baño para pájaros pequeña</p>
			<p class="card-text">$3.500</p>
		<h3 " "</h3>
				</div>
				<div class="card-footer text-center">
					<a href="#" class="btn btn-dark">Comprar</a>
				</div>
			</div>
			<div class="card" align="center">
				<div class="text-center">
					<img src="./imagenes/6.jpg" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
				</div>
				<div class="card-block">
					<h4 class="card-title">Comida roedores</h4>
			<p class="card-text">Comida para hamster y gerbos bolsa 500 gr.</p>
			<p class="card-text">$1.200</p>
		<h3 " "</h3>
				</div>
				<div class="card-footer text-center">
					<a href="#" class="btn btn-dark">Comprar</a>
				</div>
			</div>
			<div class="card" align="center">
				<div class="text-center">
					<img src="./imagenes/7.jpg" alt="" class="img-fluid yoimagen rounded-square" height="300%" width="150%">
				</div>
				<div class="card-block">
					<h4 class="card-title">Comida de conejo</h4>
					<p class="card-text">Comida para conejo adulto 300 gr</p>
					<p class="card-text">$5.400</p>
		<h3 " "</h3>
				</div>
				<div class="card-footer text-center">
					<a href="#" class="btn btn-dark">Comprar</a>
				</div>
			</div>
		 </div>
		</div>
		
	<br>	
		<br>
		<br>
		<br>

        <footer class="page-footer bg-dark" >
          <div class="container" align="center">
			  <br>
			  <br>		  
				<FONT COLOR="white"><h5 class="white-text">Si tienes alguna duda, consulta o sugerencia no dudes en llamarnos al 222266339 o escríbenos al mail contacto@patitas.cl y nos contactaremos contigo a la brevedad. <br> <a class="grey-text text-lighten-3" href="#!">Conócenos</a></h5></FONT>
				          
          </div>
          
          
           <br>
		   <br>
        </footer> 
        

    </body> 
</html> 
