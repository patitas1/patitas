/*==============================================================*/
/* DBMS name:      PostgreSQL 8                                 */
/* Created on:     27-06-2021 4:57:59                           */
/*==============================================================*/


drop index CIUDAD_PK;

drop table CIUDAD;

drop index EFECTUA_FK;

drop index REALIZA_FK;

drop index TIENE_FK;

drop index COMPRA_PK;

drop table COMPRA;

drop index MASCOTA_PK;

drop table MASCOTA;

drop index OFERTA_PK;

drop table OFERTA;

drop index PERFIL_PK;

drop table PERFIL;

drop index ESTA_FK;

drop index PERTENECE_FK;

drop index PRODUCTO_PK;

drop table PRODUCTO;

drop index SE_UBICA2_FK;

drop index SE_UBICA_FK;

drop index SE_UBICA_PK;

drop table SE_UBICA;

drop index SUCURSAL_PK;

drop table SUCURSAL;

drop index TIPO_PAGO_PK;

drop table TIPO_PAGO;

drop index VIVE_FK;

drop index CREA_FK;

drop index USUARIO_PK;

drop table USUARIO;

drop index VENDE2_FK;

drop index VENDE_FK;

drop index VENDE_PK;

drop table VENDE;

/*==============================================================*/
/* Table: CIUDAD                                                */
/*==============================================================*/
create table CIUDAD (
   ID_CIUDAD            SERIAL               not null,
   NOMBRE               VARCHAR(1024)        not null,
   constraint PK_CIUDAD primary key (ID_CIUDAD)
);

/*==============================================================*/
/* Index: CIUDAD_PK                                             */
/*==============================================================*/
create unique index CIUDAD_PK on CIUDAD (
ID_CIUDAD
);

/*==============================================================*/
/* Table: COMPRA                                                */
/*==============================================================*/
create table COMPRA (
   FOLIO                SERIAL               not null,
   CORREO               VARCHAR(1024)        not null,
   ID_PAGO              INT4                 not null,
   CORREO_SUC           VARCHAR(1024)        not null,
   FECHA                DATE                 not null,
   TOTAL_               INT4                 not null,
   DELIVERY             BOOL                 not null,
   constraint PK_COMPRA primary key (FOLIO)
);

/*==============================================================*/
/* Index: COMPRA_PK                                             */
/*==============================================================*/
create unique index COMPRA_PK on COMPRA (
FOLIO
);

/*==============================================================*/
/* Index: TIENE_FK                                              */
/*==============================================================*/
create  index TIENE_FK on COMPRA (
ID_PAGO
);

/*==============================================================*/
/* Index: REALIZA_FK                                            */
/*==============================================================*/
create  index REALIZA_FK on COMPRA (
CORREO
);

/*==============================================================*/
/* Index: EFECTUA_FK                                            */
/*==============================================================*/
create  index EFECTUA_FK on COMPRA (
CORREO_SUC
);

/*==============================================================*/
/* Table: MASCOTA                                               */
/*==============================================================*/
create table MASCOTA (
   ID_MASCOTA           SERIAL               not null,
   NOMBRE               VARCHAR(1024)        not null,
   IMA_CATEGORIA        VARCHAR(1024)        not null,
   IMA_BANNER           VARCHAR(1024)        not null,
   constraint PK_MASCOTA primary key (ID_MASCOTA)
);

/*==============================================================*/
/* Index: MASCOTA_PK                                            */
/*==============================================================*/
create unique index MASCOTA_PK on MASCOTA (
ID_MASCOTA
);

/*==============================================================*/
/* Table: OFERTA                                                */
/*==============================================================*/
create table OFERTA (
   ID_OFERTA            SERIAL               not null,
   FECHA_FIN            DATE                 not null,
   PORCENTAJE           INT4                 not null,
   constraint PK_OFERTA primary key (ID_OFERTA)
);

/*==============================================================*/
/* Index: OFERTA_PK                                             */
/*==============================================================*/
create unique index OFERTA_PK on OFERTA (
ID_OFERTA
);

/*==============================================================*/
/* Table: PERFIL                                                */
/*==============================================================*/
create table PERFIL (
   ID_PERFIL            SERIAL               not null,
   NOMBRE               VARCHAR(1024)        not null,
   constraint PK_PERFIL primary key (ID_PERFIL)
);

/*==============================================================*/
/* Index: PERFIL_PK                                             */
/*==============================================================*/
create unique index PERFIL_PK on PERFIL (
ID_PERFIL
);

/*==============================================================*/
/* Table: PRODUCTO                                              */
/*==============================================================*/
create table PRODUCTO (
   ID_PRODUCTO          SERIAL               not null,
   ID_OFERTA            INT4                 null,
   ID_MASCOTA           INT4                 not null,
   NOMBRE               VARCHAR(1024)        not null,
   PRECIO               INT4                 not null,
   STOCK                INT4                 not null,
   DESCRIPCION          VARCHAR(1024)        not null,
   IMA_PRODUCTO         VARCHAR(1024)        not null,

   constraint PK_PRODUCTO primary key (ID_PRODUCTO)
);

/*==============================================================*/
/* Index: PRODUCTO_PK                                           */
/*==============================================================*/
create unique index PRODUCTO_PK on PRODUCTO (
ID_PRODUCTO
);

/*==============================================================*/
/* Index: PERTENECE_FK                                          */
/*==============================================================*/
create  index PERTENECE_FK on PRODUCTO (
ID_MASCOTA
);

/*==============================================================*/
/* Index: ESTA_FK                                               */
/*==============================================================*/
create  index ESTA_FK on PRODUCTO (
ID_OFERTA
);

/*==============================================================*/
/* Table: SE_UBICA                                              */
/*==============================================================*/
create table SE_UBICA (
   CORREO_SUC           VARCHAR(1024)        not null,
   ID_CIUDAD            INT4                 not null,
   constraint PK_SE_UBICA primary key (CORREO_SUC, ID_CIUDAD)
);

/*==============================================================*/
/* Index: SE_UBICA_PK                                           */
/*==============================================================*/
create unique index SE_UBICA_PK on SE_UBICA (
CORREO_SUC,
ID_CIUDAD
);

/*==============================================================*/
/* Index: SE_UBICA_FK                                           */
/*==============================================================*/
create  index SE_UBICA_FK on SE_UBICA (
CORREO_SUC
);

/*==============================================================*/
/* Index: SE_UBICA2_FK                                          */
/*==============================================================*/
create  index SE_UBICA2_FK on SE_UBICA (
ID_CIUDAD
);

/*==============================================================*/
/* Table: SUCURSAL                                              */
/*==============================================================*/
create table SUCURSAL (
   CORREO_SUC           VARCHAR(1024)        not null,
   UBICACION            VARCHAR(1024)        not null,
   TELEFONO             VARCHAR(1024)        not null,
   IMA_MAPA             VARCHAR(1024)        not null,
   constraint PK_SUCURSAL primary key (CORREO_SUC)
);

/*==============================================================*/
/* Index: SUCURSAL_PK                                           */
/*==============================================================*/
create unique index SUCURSAL_PK on SUCURSAL (
CORREO_SUC
);

/*==============================================================*/
/* Table: TIPO_PAGO                                             */
/*==============================================================*/
create table TIPO_PAGO (
   ID_PAGO              SERIAL               not null,
   NOMBRE               VARCHAR(1024)        not null,
   constraint PK_TIPO_PAGO primary key (ID_PAGO)
);

/*==============================================================*/
/* Index: TIPO_PAGO_PK                                          */
/*==============================================================*/
create unique index TIPO_PAGO_PK on TIPO_PAGO (
ID_PAGO
);

/*==============================================================*/
/* Table: USUARIO                                               */
/*==============================================================*/
create table USUARIO (
   CORREO               VARCHAR(1024)        not null,
   ID_PERFIL            INT4                 not null,
   ID_CIUDAD            INT4                 not null,
   RUT                  VARCHAR(1024)        null,
   NOMBRE               VARCHAR(1024)        not null,
   CONTRASENA           VARCHAR(1024)        not null,
   DIRECCION            VARCHAR(1024)        not null,
   constraint PK_USUARIO primary key (CORREO)
);

/*==============================================================*/
/* Index: USUARIO_PK                                            */
/*==============================================================*/
create unique index USUARIO_PK on USUARIO (
CORREO
);

/*==============================================================*/
/* Index: CREA_FK                                               */
/*==============================================================*/
create  index CREA_FK on USUARIO (
ID_PERFIL
);

/*==============================================================*/
/* Index: VIVE_FK                                               */
/*==============================================================*/
create  index VIVE_FK on USUARIO (
ID_CIUDAD
);

/*==============================================================*/
/* Table: VENDE                                                 */
/*==============================================================*/
create table VENDE (
   ID_PRODUCTO          INT4                 not null,
   FOLIO                INT4                 not null,
   CANTIDAD             INT4                 not null,
   PRECIO_VENTA         INT4                 not null,
   constraint PK_VENDE primary key (ID_PRODUCTO, FOLIO)
);

/*==============================================================*/
/* Index: VENDE_PK                                              */
/*==============================================================*/
create unique index VENDE_PK on VENDE (
ID_PRODUCTO,
FOLIO
);

/*==============================================================*/
/* Index: VENDE_FK                                              */
/*==============================================================*/
create  index VENDE_FK on VENDE (
ID_PRODUCTO
);

/*==============================================================*/
/* Index: VENDE2_FK                                             */
/*==============================================================*/
create  index VENDE2_FK on VENDE (
FOLIO
);

alter table COMPRA
   add constraint FK_COMPRA_EFECTUA_SUCURSAL foreign key (CORREO_SUC)
      references SUCURSAL (CORREO_SUC)
      on delete restrict on update restrict;

alter table COMPRA
   add constraint FK_COMPRA_REALIZA_USUARIO foreign key (CORREO)
      references USUARIO (CORREO)
      on delete restrict on update restrict;

alter table COMPRA
   add constraint FK_COMPRA_TIENE_TIPO_PAG foreign key (ID_PAGO)
      references TIPO_PAGO (ID_PAGO)
      on delete restrict on update restrict;

alter table PRODUCTO
   add constraint FK_PRODUCTO_ESTA_OFERTA foreign key (ID_OFERTA)
      references OFERTA (ID_OFERTA)
      on delete restrict on update restrict;

alter table PRODUCTO
   add constraint FK_PRODUCTO_PERTENECE_MASCOTA foreign key (ID_MASCOTA)
      references MASCOTA (ID_MASCOTA)
      on delete restrict on update restrict;

alter table SE_UBICA
   add constraint FK_SE_UBICA_SE_UBICA_SUCURSAL foreign key (CORREO_SUC)
      references SUCURSAL (CORREO_SUC)
      on delete restrict on update restrict;

alter table SE_UBICA
   add constraint FK_SE_UBICA_SE_UBICA2_CIUDAD foreign key (ID_CIUDAD)
      references CIUDAD (ID_CIUDAD)
      on delete restrict on update restrict;

alter table USUARIO
   add constraint FK_USUARIO_CREA_PERFIL foreign key (ID_PERFIL)
      references PERFIL (ID_PERFIL)
      on delete restrict on update restrict;

alter table USUARIO
   add constraint FK_USUARIO_VIVE_CIUDAD foreign key (ID_CIUDAD)
      references CIUDAD (ID_CIUDAD)
      on delete restrict on update restrict;

alter table VENDE
   add constraint FK_VENDE_VENDE_PRODUCTO foreign key (ID_PRODUCTO)
      references PRODUCTO (ID_PRODUCTO)
      on delete restrict on update restrict;

alter table VENDE
   add constraint FK_VENDE_VENDE2_COMPRA foreign key (FOLIO)
      references COMPRA (FOLIO)
      on delete restrict on update restrict;

