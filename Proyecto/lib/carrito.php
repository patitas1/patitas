<?php
    session_start();
    require_once("../lib/compartido.php");


    if (isset($_REQUEST['accion'])) {
        $conn = conectarBD();
  
        switch ($_REQUEST['accion']) {
            case 1:
                carrito();
                break;  
        }

    }

function carrito(){
    $titulo = $_REQUEST['nombre'];
    $imagen = $_REQUEST['imagen'];
    $descripcion = $_REQUEST['descripcion'];
    $precio = $_REQUEST['precio'];
    $lugar = $_REQUEST['lugar'];
    $nombre_categoria = $_REQUEST['nombre_categoria'];
    $imagen_categoria = $_REQUEST['imagen_categoria'];
    
	if (isset($_SESSION["carrito"])) {
        $carrito = $_SESSION["carrito"];
        $carrito[] = array("nombre"=>$titulo, "precio"=>$precio, "imagen"=>$imagen);

        $_SESSION["carrito"] = $carrito;

	    $res["datos"] = $_SESSION["carrito"];
	    $res["mensaje"] = "Se agrego al carrito";
	    $res["salida_exitosa"] = true;

        if($lugar == "patitas"){
	        $res["ubicacion"] = "../index/patitas.php";
        }

        else{
	        $res["ubicacion"] = "../vista/productos_filtrados.php?nombre=" . $nombre_categoria . "&i=" . $imagen_categoria;
        }

        echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"], "ubicacion"=>$res["ubicacion"])); 
    }

    else{
        $_SESSION["tmp"][] = array("nombre"=>$titulo, "precio"=>$precio, "imagen"=>$imagen);

        #$_SESSION["tmp"] = $tmp;

	    $res["datos"] = $_SESSION["tmp"];
	    $res["mensaje"] = "Sin iniciar sesión";
	    $res["salida_exitosa"] = true;

        if($lugar == "patitas"){
	        $res["ubicacion"] = "../index/patitas.php";
        }

        else{
	        $res["ubicacion"] = "../vista/productos_filtrados.php?nombre=" . $nombre_categoria . "&i=" . $imagen_categoria;
        }

        echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"], "ubicacion"=>$res["ubicacion"])); 
    }

}
    
?>
