<?php
	session_start();
	require('../lib/compartido.php');
    
    // Indica que no se ha iniciado la sesion
    $_SESSION['registro_patitas'] = false;
    
    // Variables para para la salida del json
    $salida_exitosa = false;
    $mensaje = "Error de usuario o clave";
    $ubicacion = "../lib/cerrar_sesion.php";

    $conn = conectarBD();

    if ($conn) {
        if (isset($_POST['mail'], $_POST['pwd'])) {
            if ($_POST['mail']!="" and $_POST['mail']!="") {
                
                // Variables con la informacion del formulario
                $correo = $_POST['mail'];
                $contrasena = $_POST['pwd'];
                $encriptar_pwd = encriptar($contrasena);

                $sql = "select usuario.correo, usuario.rut, usuario.nombre, ".
                "usuario.contrasena, usuario.direccion, perfil.nombre as nombre_per, ".
                "ciudad.nombre as nombre_ciu from usuario inner join perfil on ".
                "usuario.id_perfil = perfil.id_perfil inner join ciudad on ".
                "usuario.id_ciudad = ciudad.id_ciudad where correo = :correo and contrasena = :contrasena";

                $stmt = $conn->prepare($sql);
                $stmt->bindValue(':correo', $correo);
                $stmt->bindValue(':contrasena', $encriptar_pwd);

                // Verifica si se ejecuto el sql
                if ($stmt->execute()) {
                    $array_session = $stmt->fetchAll(\PDO::FETCH_ASSOC);
                    
                    // Continua si la ejecucion fue correcta
                    if (count($array_session) == 1) {
                        // Definicion de variables de sesion
                        $_SESSION['correo'] = $array_session[0]["correo"];
                        $_SESSION['contrasena'] = $array_session[0]["contrasena"];
                        $_SESSION['rut'] = $array_session[0]["rut"];
                        $_SESSION['nombre'] = $array_session[0]["nombre"];
                        $_SESSION['direccion'] = $array_session[0]["direccion"];
                        $_SESSION['perfil'] = $array_session[0]["nombre_per"];
                        $_SESSION['ciudad'] = $array_session[0]["nombre_ciu"];

                        // Ahora esta iniciada la sesion
                        $_SESSION['registro_patitas'] = true;
                        $_SESSION['carrito'] = [];
                        $_SESSION['num_producto'] = 0;

                        // Variables de una salida exitosa
                        $mensaje = "ok";
                        if ($_SESSION['perfil'] == 'administrador') {
							$ubicacion = "../vista/administrador.php";
						}
						else{
							$ubicacion = "../index/patitas.php";
						}
						$salida_exitosa = true;
                    }
                    
                    else{
                        $mensaje = "Usuario o Clave erróneos.";
                    }
                }
                
                else{
                    $mensaje = "Error al ejecutar la consulta.";
                }
            }
            
            else{
                $mensaje = "Todos los datos son requeridos.";
            }
        }

        else{
            $msg = "Todos los datos son requeridos.";
        }
    }

    else{
        $mensaje = "No puede conectar a la Base de Datos.";
    }

    // Salida que se ejecutara en el archivo ingresar.js
    $json_salida = array('salida_exitosa' => $salida_exitosa, 'mensaje' => $mensaje, 'ubicacion'=> $ubicacion);
    echo json_encode($json_salida);
?>
