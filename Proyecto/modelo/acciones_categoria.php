<?php
    session_start();
    require_once("../lib/compartido.php");
    validarSesion();


    if (isset($_REQUEST['accion'])) {
        $conn = conectarBD();
  
        switch ($_REQUEST['accion']) {
            case 1:
                seleccionar($conn);
                break;  

            case 2:
                insertar($conn);
                break;

	        case 3:
                eliminar($conn);
                break;

	        case 4:
                seleccionarUno ($conn);
                break;

            case 5:
                actualizar($conn);
                break;

	        case 6: 
                borrar_archivo($conn);
                break; 

	        case 7: 
                editar_icono($conn);
                break; 

	        case 8: 
                editar_banner($conn);
                break; 
        }  
    }

function seleccionar ($conn) {
    $sql= " select id_mascota, nombre, ima_categoria, ima_banner from mascota;";
  
    $stmt = $conn->prepare($sql);

    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function insertar($conn) {
    $nombre = $_REQUEST['nombre'];
    $icono = $_REQUEST['icono'];
    $banner = $_REQUEST['banner'];

  
    $sql = "insert into mascota(nombre, ima_categoria, ima_banner) values(:nombre, :icono, :banner)";

    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':nombre', $nombre);
    $stmt->bindValue(':icono', $icono);
    $stmt->bindValue(':banner', $banner);
  
    $res = ejecutarSQL($stmt);
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function eliminar($conn) {
    $id_categoria = $_REQUEST['id_categoria'];

    $sql = "delete from mascota where id_mascota = :id_categoria";

    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':id_categoria', $id_categoria);
    $res = ejecutarSQL($stmt);
  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function seleccionarUno ($conn) {
    $id_categoria = $_REQUEST['id_categoria'];
    $sql= "select nombre from mascota where id_mascota = :id_categoria";
  
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':id_categoria', $id_categoria);  
    
    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function actualizar ($conn) {
    $id_categoria = $_REQUEST['id_categoria'];
    $nombre = $_REQUEST['nombre'];
 
    $sql = "update mascota set nombre = :nombre where id_mascota = :id_categoria;";
  
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':id_categoria', $id_categoria);
    $stmt->bindValue(':nombre', $nombre);

    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function borrar_archivo($conn){
    $path =  "../vista/imagenes/";

    if (isset($_REQUEST['archivo'])) {
        $archivo = $_REQUEST['archivo'];
        @unlink($path.$archivo);
        echo json_encode(array("mensaje"=>"Se elimino")); 
    }
}

function editar_icono ($conn) {
    $id_categoria = $_REQUEST['id_categoria'];
    $archivo = $_REQUEST['archivo'];
 
    $sql = "update mascota set ima_categoria = :archivo where id_mascota = :id_categoria";
  
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':id_categoria', $id_categoria);
    $stmt->bindValue(':archivo', $archivo);

    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function editar_banner ($conn) {
    $id_categoria = $_REQUEST['id_categoria'];
    $archivo = $_REQUEST['archivo'];
 
    $sql = "update mascota set ima_banner = :archivo where id_mascota = :id_categoria";
  
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':id_categoria', $id_categoria);
    $stmt->bindValue(':archivo', $archivo);

    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}
?>
