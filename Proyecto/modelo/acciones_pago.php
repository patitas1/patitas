<?php
    session_start();
    require_once("../lib/compartido.php");
    validarSesion();

    if (isset($_REQUEST['accion'])) {
        $conn = conectarBD();
  
        switch ($_REQUEST['accion']) {
            case 1:
                # select 
                seleccionar($conn);
                break;  
            case 2:
                # insertar
                insertar($conn);
                break;
	        case 3:
                # delete
                eliminar($conn);
                break;
	        case 4:
                # select where = ?
                seleccionarUno ($conn);
                break;
            case 5:
                # update
                actualizar($conn);
                break;
        }  
    }

function seleccionar ($conn) {
    $sql= "select * from tipo_pago";
  
    $stmt = $conn->prepare($sql);

    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function insertar($conn) {
    $nombre = trim($_REQUEST['nombre']);

    $sql = "insert into tipo_pago(nombre) values(:nombre)";

    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':nombre', $nombre);
  
    $res = ejecutarSQL($stmt);
  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function eliminar($conn) {
    $id_pago = $_REQUEST['id_pago'];

    $sql = "delete from tipo_pago where id_pago = :id_pago;";

    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':id_pago', $id_pago);
    $res = ejecutarSQL($stmt);
  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function actualizar ($conn) {
    $id_pago = trim($_REQUEST['id_pago']);
    $nombre = trim($_REQUEST['nombre']);
 
    $sql = "update tipo_pago set nombre = :nombre where id_pago = :id_pago;";
  
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':id_pago', $id_pago);
    $stmt->bindValue(':nombre', $nombre);

    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function seleccionarUno ($conn) {
    $id_pago = $_REQUEST['id_pago'];
    $sql= "select nombre from tipo_pago where id_pago = :id_pago";
  
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':id_pago', $id_pago);  
    
    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

?>
