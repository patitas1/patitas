<?php
    session_start();
    require_once("../lib/compartido.php");
    validarSesion();

    if (isset($_REQUEST['accion'])) {
        $conn = conectarBD();
  
        switch ($_REQUEST['accion']) {
            case 1:
				seleccionar($conn);
				break;  
            case 2:
                insertar($conn);
                break;
	        case 3:
                eliminar($conn);
                break;
	        case 4:
                seleccionarUno ($conn);
                break;
            case 5:
                actualizar($conn);
                break;
            case 6: 
				seleccionar_perfil($conn);
				break;
			case 7: 
				obtener_id($conn);
				break;
        }  
    }

function insertar($conn) {
    $rut = $_REQUEST['rut'];
    $usuario = $_REQUEST['usuario'];
    $email = $_REQUEST['email'];
    $contrasena = $_REQUEST['contrasena'];
    $direccion = $_REQUEST['direccion'];
    $id_ciudad = $_REQUEST['id_ciudad'];
    $id_perfil = $_REQUEST['id_perfil'];
    
    $encriptar_contra = encriptar($contrasena);

    $sql = "insert into usuario (correo, id_perfil, id_ciudad, rut, nombre, contrasena, direccion) " .
            "values (:email, :id_perfil, :id_ciudad, :rut, :nombre, :contrasena, :direccion) ";

    $stmt = $conn->prepare($sql);

    $stmt->bindValue(':email', $email); 
    $stmt->bindValue(':id_ciudad', $id_ciudad); 
    $stmt->bindValue(':id_perfil', $id_perfil); 
    $stmt->bindValue(':rut', $rut); 
    $stmt->bindValue(':nombre', $usuario); 
    $stmt->bindValue(':contrasena', $encriptar_contra); 
    $stmt->bindValue(':direccion', $direccion); 

    $res = ejecutarSQL($stmt);
  
     echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function seleccionar ($conn) {
    $sql= "select usuario.nombre as usuario, rut, usuario.correo, direccion, ciudad.nombre as ciudad, perfil.nombre as " .
        "perfil, usuario.id_perfil from usuario inner join ciudad on ciudad.id_ciudad = usuario.id_ciudad inner join perfil " .
        "on usuario.id_perfil = perfil.id_perfil";
  
    $stmt = $conn->prepare($sql);

    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function seleccionar_perfil($conn) {
    $sql= "select id_perfil, nombre from perfil;";
	
    $stmt = $conn->prepare($sql);
    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}


function obtener_id($conn) {
    $nom_perfil = $_REQUEST['nombre'];

    $sql= "select id_perfil from perfil where nombre = :nom_perfil;";
	
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':nom_perfil', $nom_perfil); 
    $res = ejecutarSQL($stmt);  

    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function eliminar($conn) {
    $correo = $_REQUEST['correo'];

    $sql = "delete from usuario where correo = :correo;";

    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':correo', $correo);
    $res = ejecutarSQL($stmt);
  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function seleccionarUno ($conn) {
    $correo = $_REQUEST['correo'];
    $sql= "select usuario.nombre as usuario, rut, contrasena, usuario.correo, direccion, ciudad.nombre as ciudad, ". 
        "perfil.nombre as perfil, usuario.id_perfil from usuario inner join ciudad on ciudad.id_ciudad = usuario.id_ciudad " .
        "inner join perfil on usuario.id_perfil = perfil.id_perfil where correo = :correo";
  
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':correo', $correo);  
    $res = ejecutarSQL($stmt);  

    $contrasena = $res["datos"][0]["contrasena"];
    $desencriptada = desencriptar($contrasena);

    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"], "contra"=>$desencriptada));
}

function actualizar ($conn) {
    $rut = $_REQUEST['rut'];
    $usuario = $_REQUEST['usuario'];
    $email = $_REQUEST['nuevo'];
    $contrasena = $_REQUEST['contrasena'];
    $direccion = $_REQUEST['direccion'];
    $id_ciudad = $_REQUEST['id_ciudad'];
    $id_perfil = $_REQUEST['id_perfil'];
    $antiguo = $_REQUEST['antiguo'];
 
    $encriptar_contra = encriptar($contrasena);

    $sql = "update usuario set correo = :nuevo, id_perfil = :id_perfil, id_ciudad = :id_ciudad, rut = :rut, nombre = :usuario, " . 
            "contrasena = :contrasena, direccion = :direccion  where correo = :antiguo";
  
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':nuevo', $email);
    $stmt->bindValue(':antiguo', $antiguo);
    $stmt->bindValue(':id_perfil', $id_perfil);
    $stmt->bindValue(':id_ciudad', $id_ciudad);
    $stmt->bindValue(':rut', $rut);
    $stmt->bindValue(':usuario', $usuario);
    $stmt->bindValue(':contrasena', $encriptar_contra);
    $stmt->bindValue(':direccion', $direccion);

    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}
?>
