<?php
    require_once("../lib/compartido.php");

    if (isset($_REQUEST['accion'])) {
        $conn = conectarBD();
  
        switch ($_REQUEST['accion']) {
            case 1:
                seleccionar($conn);
                break;  
            case 2:
                traer_categorias($conn);
                break;
        }

    }

function seleccionar ($conn) {
    $filtro = $_REQUEST['filtro'];
    
    $sql= "select * from producto where lower(descripcion) like :filtro";
    
    $filtro = "%".strtolower($filtro)."%";
  
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':filtro', $filtro); 
    $res = ejecutarSQL($stmt);  

    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}


function traer_categorias ($conn) {
    $sql= "select * from mascota";
    
    $stmt = $conn->prepare($sql);
    $res = ejecutarSQL($stmt);  

    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

?>
