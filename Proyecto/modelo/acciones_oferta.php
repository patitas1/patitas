<?php
    session_start();
    require_once("../lib/compartido.php");
    validarSesion();

    if (isset($_REQUEST['accion'])) {
        $conn = conectarBD();
  
        switch ($_REQUEST['accion']) {
            case 1:
                seleccionar($conn);
                break;  
            case 2:
                insertar($conn);
                break;
	        case 3:
                eliminar($conn);
                break;
	        case 4:
                seleccionarUno ($conn);
                break;
            case 5:
                actualizar($conn);
                break;
        }  
    }

function seleccionar ($conn) {
    $sql= "select * from oferta";
  
    $stmt = $conn->prepare($sql);

    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function insertar($conn) {
    $porcentaje = $_REQUEST['porcentaje'];
    $fecha = $_REQUEST['fecha'];

    $sql = "insert into oferta(fecha_fin, porcentaje) values (:fecha, :porcentaje)";

    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':porcentaje', $porcentaje);
    $stmt->bindValue(':fecha', $fecha);
  
    $res = ejecutarSQL($stmt);
  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function eliminar($conn) {
    $id_oferta = $_REQUEST['id_oferta'];

    $sql = "delete from oferta where id_oferta = :id_oferta";

    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':id_oferta', $id_oferta);
    $res = ejecutarSQL($stmt);
  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function actualizar ($conn) {
  $id_oferta = $_REQUEST['id_oferta'];
  $porcentaje = $_REQUEST['porcentaje'];
  $fecha = $_REQUEST['fecha'];
 
  $sql = "update oferta set porcentaje = :porcentaje, fecha_fin = :fecha where id_oferta = :id_oferta";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_oferta', $id_oferta);
  $stmt->bindValue(':fecha', $fecha);
  $stmt->bindValue(':porcentaje', $porcentaje);

  $res = ejecutarSQL($stmt);  
  echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function seleccionarUno ($conn) {
  $id_oferta = $_REQUEST['id_oferta'];

  $sql= "select porcentaje, fecha_fin from oferta where id_oferta = :id_oferta";
  
  $stmt = $conn->prepare($sql);
  $stmt->bindValue(':id_oferta', $id_oferta);  
    
  $res = ejecutarSQL($stmt);  
  echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}
?>
