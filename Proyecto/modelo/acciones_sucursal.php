<?php
    session_start();
    require_once("../lib/compartido.php");
    validarSesion();


    if (isset($_REQUEST['accion'])) {
        $conn = conectarBD();
  
        switch ($_REQUEST['accion']) {
            case 1:
                seleccionar($conn);
                break;  

            case 2:
                insertar($conn);
                insertar_ubicacion($conn);
                break;

	        case 3:
                eliminar($conn);
                break;

	        case 4:
                seleccionarUno ($conn);
                break;

            case 5:
                actualizar($conn);
                actualizar_ciudad($conn);
                break;

	        case 6: 
                seleccionar_ciudad($conn);
                break; 

	        case 7: 
                borrar_archivo($conn);
                break; 

	        case 8: 
                editar_mapa($conn);
                break; 
        }  
    }

function seleccionar ($conn) {
    $sql= " select ciudad.nombre as nombre_ciu, ubicacion, telefono, sucursal.correo_suc, " .
        "ima_mapa from sucursal inner join se_ubica on se_ubica.correo_suc = sucursal.correo_suc " .
        "inner join ciudad on se_ubica.id_ciudad = ciudad.id_ciudad";
  
    $stmt = $conn->prepare($sql);

    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function insertar($conn) {
    $correo = trim($_REQUEST['correo']);
    $ubicacion = trim($_REQUEST['direccion']);
    $telefono = trim($_REQUEST['telefono']);
    $archivo = trim($_REQUEST['archivo']);
  
    $sql = "insert into sucursal(correo_suc, ubicacion, telefono, ima_mapa) values(:correo, :direccion, :telefono, :archivo)";

    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':correo', $correo);
    $stmt->bindValue(':direccion', $ubicacion);
    $stmt->bindValue(':telefono', $telefono);
    $stmt->bindValue(':archivo', $archivo);
  
    $res = ejecutarSQL($stmt);
}

function insertar_ubicacion($conn) {
    $correo = trim($_REQUEST['correo']);
    $ciudad = trim($_REQUEST['id_ciudad']);
 
    $sql = "insert into se_ubica(correo_suc, id_ciudad) values(:correo, :id_ciudad)";

    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':correo', $correo);
    $stmt->bindValue(':id_ciudad', $ciudad);

    $res = ejecutarSQL($stmt);
  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function seleccionar_ciudad ($conn) {
    $sql= "select nombre from ciudad;";
	
    $stmt = $conn->prepare($sql);
    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function eliminar($conn) {
    $correo = $_REQUEST['correo'];

    $sql = "delete from sucursal where correo_suc = :correo";

    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':correo', $correo);
    $res = ejecutarSQL($stmt);
  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function borrar_archivo($conn){
    $path =  "../vista/imagenes/";

    if (isset($_REQUEST['archivo'])) {
        $archivo = $_REQUEST['archivo'];
        @unlink($path.$archivo);
        echo json_encode(array("mensaje"=>"Se elimino")); 
    }
}

function seleccionarUno ($conn) {
    $correo = $_REQUEST['correo'];
    $sql= "select ciudad.nombre as nombre_ciu, ubicacion, telefono, sucursal.correo_suc, ima_mapa " .
        "from sucursal inner join se_ubica on se_ubica.correo_suc = sucursal.correo_suc inner join " .
        "ciudad on se_ubica.id_ciudad = ciudad.id_ciudad where sucursal.correo_suc = :correo";
  
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':correo', $correo);  
    
    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function editar_mapa ($conn) {
    $correo = $_REQUEST['correo'];
    $archivo = $_REQUEST['archivo'];
 
    $sql = "update sucursal set ima_mapa = :archivo where correo_suc = :correo";
  
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':correo', $correo);
    $stmt->bindValue(':archivo', $archivo);

    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function actualizar ($conn) {
    $nuevo = $_REQUEST['email'];
    $anterior = $_REQUEST['correo_anterior'];
    $direccion = $_REQUEST['direccion'];
    $telefono = $_REQUEST['telefono'];
 
    $sql = "update sucursal set correo_suc = :nuevo, ubicacion = :direccion, telefono = :telefono where correo_suc = :anterior";
  
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':nuevo', $nuevo);
    $stmt->bindValue(':anterior', $anterior);
    $stmt->bindValue(':direccion', $direccion);
    $stmt->bindValue(':telefono', $telefono);

    $res = ejecutarSQL($stmt);  
    //echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function actualizar_ciudad ($conn) {
    $correo = $_REQUEST['email'];
    $id_ciudad = $_REQUEST['id_ciudad'];

    $sql = "update se_ubica set id_ciudad = :id_ciudad where correo_suc = :correo";
  
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':correo', $correo);
    $stmt->bindValue(':id_ciudad', $id_ciudad);

    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}
?>
