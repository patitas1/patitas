<?php

    require_once("../lib/compartido.php");

    if (isset($_REQUEST['accion'])) {
        $conn = conectarBD();
  
        switch ($_REQUEST['accion']) {
            case 1: 
                seleccionar($conn);
                break;  
	}
}

function seleccionar($conn) {
    $correo = $_REQUEST['correo'];

    $sql = "select contrasena from usuario where correo = :correo ";

    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':correo', $correo); 
    $res = ejecutarSQL($stmt);
  
    $contrasena = $res["datos"][0]["contrasena"];
    $desencriptada = desencriptar($contrasena);

    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$desencriptada));
}
?>
