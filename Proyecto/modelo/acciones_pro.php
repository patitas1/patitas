<?php
    require_once("../lib/compartido.php");

    if (isset($_REQUEST['accion'])) {
        $conn = conectarBD();
  
        switch ($_REQUEST['accion']) {
            case 1:
                # select 
                seleccionar($conn);
                break;  
        }

    }

function seleccionar ($conn) {
    $filtro = $_REQUEST['filtro'];
    $nombre = $_REQUEST['nombre'];
    
    $sql= "select * from producto inner join mascota on producto.id_mascota = mascota.id_mascota " .
            "where lower(descripcion) like :filtro AND mascota.nombre IN (select mascota.nombre " .
            "from mascota where mascota.nombre = :nombre)";
    
    $filtro = "%".strtolower($filtro)."%";
  
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':filtro', $filtro); 
    $stmt->bindValue(':nombre', $nombre); 
    $res = ejecutarSQL($stmt);  

    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

?>
