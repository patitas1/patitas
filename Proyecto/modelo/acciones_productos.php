<?php
    session_start();
    require_once("../lib/compartido.php");
    validarSesion();


    if (isset($_REQUEST['accion'])) {
        $conn = conectarBD();
  
        switch ($_REQUEST['accion']) {
            case 1:
                seleccionar($conn);
                break;  

            case 2:
                insertar($conn);
                break;

	        case 3:
                eliminar($conn);
                break;

	        case 4:
                seleccionarUno ($conn);
                break;

            case 5:
                actualizar($conn);
                break;

	        case 6: 
                seleccionar_categoria($conn);
                break; 

	        case 7: 
                borrar_archivo($conn);
                break; 

	        case 8: 
                editar_imagen($conn);
                break; 

	        case 9: 
                obtener_id($conn);
                break; 

	        case 10: 
                seleccionar_oferta($conn);
                break; 

	        case 11: 
                aplicar_oferta($conn);
                break; 
        }
    }

function seleccionar ($conn) {
    $sql= "select id_producto, producto.nombre, precio, stock, mascota.nombre as categoria, descripcion, " .
            "ima_producto, producto.id_oferta, porcentaje, fecha_fin as fecha from producto inner join mascota " .
            "on producto.id_mascota = mascota.id_mascota left join oferta on producto.id_oferta = oferta.id_oferta";
  
    $stmt = $conn->prepare($sql);

    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function seleccionar_categoria ($conn) {
    $sql= "select nombre from mascota";
	
    $stmt = $conn->prepare($sql);
    $res = ejecutarSQL($stmt);  

    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function obtener_id($conn) {
    $nom_categoria = $_REQUEST['nombre'];

    $sql= "select id_mascota from mascota where nombre = :nom_categoria";
	
    $stmt = $conn->prepare($sql);

    $stmt->bindValue(':nom_categoria', $nom_categoria); 
    $res = ejecutarSQL($stmt);  

    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function seleccionar_oferta ($conn) {
    $sql= "select id_oferta from oferta";
	
    $stmt = $conn->prepare($sql);
    $res = ejecutarSQL($stmt);  

    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function insertar($conn) {
    $nombre = $_REQUEST['nombre'];
    $descripcion =$_REQUEST['descripcion'];
    $precio = $_REQUEST['precio'];
    $stock = $_REQUEST['stock'];
    $archivo = $_REQUEST['archivo'];
    $id_categoria = $_REQUEST['id_categoria'];
  
    $sql = "insert into producto(nombre, precio, stock, descripcion, ima_producto, id_mascota) values(:nombre, :precio, :stock, :descripcion, :archivo, :id_categoria)";

    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':nombre', $nombre);
    $stmt->bindValue(':descripcion', $descripcion);
    $stmt->bindValue(':precio', $precio);
    $stmt->bindValue(':stock', $stock);
    $stmt->bindValue(':archivo', $archivo);
    $stmt->bindValue(':id_categoria', $id_categoria);
  
    $res = ejecutarSQL($stmt);
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function eliminar($conn) {
    $id_producto = $_REQUEST['id_producto'];
    
    $sql = "delete from producto where id_producto = :id_producto";

    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':id_producto', $id_producto);
    $res = ejecutarSQL($stmt);
  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function seleccionarUno ($conn) {
    $id_producto = $_REQUEST['id_producto'];

    $sql= "select producto.nombre, precio, stock, descripcion, mascota.nombre as categoria from producto inner join mascota on " .
            "producto.id_mascota = mascota.id_mascota where id_producto = :id_producto";
  
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':id_producto', $id_producto);  
    
    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"]));
}

function actualizar ($conn) {
    $nombre = $_REQUEST['nombre'];
    $descripcion =$_REQUEST['descripcion'];
    $precio = $_REQUEST['precio'];
    $stock = $_REQUEST['stock'];
    $id_categoria = $_REQUEST['id_categoria'];
    $id_producto = $_REQUEST['id_producto'];
    
    $sql = "update producto set nombre = :nombre, descripcion = :descripcion, precio = :precio, id_mascota = :id_categoria, stock = :stock where id_producto = :id_producto";
  
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':nombre', $nombre);
    $stmt->bindValue(':descripcion', $descripcion);
    $stmt->bindValue(':precio', $precio);
    $stmt->bindValue(':stock', $stock);
    $stmt->bindValue(':id_categoria', $id_categoria);
    $stmt->bindValue(':id_producto', $id_producto);

    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function aplicar_oferta ($conn) {
    $id_oferta = $_REQUEST['id_oferta'];
    $id_producto = $_REQUEST['id_producto'];
    
    $sql = "update producto set id_oferta = :id_oferta where id_producto = :id_producto";
  
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':id_oferta', $id_oferta);
    $stmt->bindValue(':id_producto', $id_producto);

    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}

function borrar_archivo($conn){
    $path =  "../vista/imagenes/";

    if (isset($_REQUEST['archivo'])) {
        $archivo = $_REQUEST['archivo'];
        @unlink($path.$archivo);
        echo json_encode(array("mensaje"=>"Se elimino")); 
    }
}

function editar_imagen ($conn) {
    $id_producto = $_REQUEST['id_producto'];
    $archivo = $_REQUEST['archivo'];
 
    $sql = "update producto set ima_producto = :archivo where id_producto = :id_producto";
  
    $stmt = $conn->prepare($sql);
    $stmt->bindValue(':id_producto', $id_producto);
    $stmt->bindValue(':archivo', $archivo);

    $res = ejecutarSQL($stmt);  
    echo json_encode(array("salida_exitosa"=>$res["salida_exitosa"], "mensaje"=>$res["mensaje"], "datos"=>$res["datos"])); 
}
?>
