<?php
    session_start();
    require_once("../lib/compartido.php");
    validarSesion();

    # https://www.php.net/manual/en/features.file-upload.errors.php
    $phpFileUploadErrors = array(
        0 => 'There is no error, the file uploaded with success',
        1 => 'The uploaded file exceeds the upload_max_filesize directive in php.ini',
        2 => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form',
        3 => 'The uploaded file was only partially uploaded',
        4 => 'No file was uploaded',
        6 => 'Missing a temporary folder',
        7 => 'Failed to write file to disk.',
        8 => 'A PHP extension stopped the file upload.',
    );

    # carpeta con imágenes. ver propieratio de la carpeta, debe ser www-data.
    $path =  "../vista/imagenes/";

    # obtiene solo el nombre del archivo.
    $filename = basename($_FILES['icono']['name']);

    # concatena la ruta donde se subirá el archivo.
    $uploadFile = $path . $filename;

    # mueve el archivo a la ruta definida.
    if (move_uploaded_file($_FILES['icono']['tmp_name'], $uploadFile)) {
        echo json_encode(array("estado"=>"ok", "mensaje"=> "Archivo subido", "nom_archivo"=>$filename));
    } 

    else {
    
        $error_code = $_FILES['icono']['error'];
        $error = $phpFileUploadErrors[$error_code];
    
        echo json_encode(array("estado"=>"error", "mensaje"=>"No se pudo subir el archivo: " . $error));
    }

?>
