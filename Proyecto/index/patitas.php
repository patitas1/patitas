<?php
	require_once("../lib/compartido.php");
?>

<!DOCTYPE html> 
<html>

	<head>
		
		<title> Patitas Tienda de Mascotas </title>
		<?php
			head();
		?>
		
    </head>

    <body style="background-color:#f2f2f2;">

		<?php
			navbar();
		?>
     
		<!-- Titulo --> 
		<div class="container p-5 my-5" align="center">
			<img align="center" src="../vista/imagenes/patitas6.png" width="55%">
		</div>


		<div id="categoria" name="categoria" class="container" align="center"> </div>	

        <!-- Tabla -->
		<br><br><br><br>
        <div class="container">
		    <div class="tabla"> 
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="tabla_productos" class="display table compact nowrap"></table>
                            </div>
                        </div> 
                    </div> 
                </div>
            </div>  
        </div>  

		<br><br><br>
    
        <!-- Footer-->        
		<footer class="page-footer bg-dark" >
			<div class="container" align="center">
				<br><br>
				<FONT COLOR="white">
                    <h5 class="white-text">
                        Si tienes alguna duda, consulta o sugerencia no dudes en llamarnos al 222266339 o escríbenos al mail contacto@patitas.cl y nos contactaremos contigo a la brevedad.
                        <br> 
                        <a class="grey-text text-lighten-3" href="../vista/sucursal.html">Conócenos</a>
                    </h5>
                </FONT>
			</div> 
            <br><br>
			<script src="../controlador/patitas.js"></script>
        </footer> 
        
    </body>
    
    <?php
		pushbar();
	?>
     
</html> 
