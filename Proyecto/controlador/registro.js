$(function(){
	obtener_ciudades();

    var id_ciudad;
    id_ciudad = obtener_id_ciudad();
});

function obtener_ciudades(){
	$.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_registro.php',
        data: {accion:2},
        success: function (response) {   
		    if (response.salida_exitosa) {
			    var items="";
				items ="<option>Seleccionar</option>";

			    $.each(response.datos,function(index,item) {
				    items+="<option>" + item["nombre"]+"</option>";
			        $("#ciudad").html(items);
			    });
            }
     
            else {
                swal('Error', response.mensaje[2], 'error');
            }
	
	    }, 

        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    }); 
} 

$("#btn_registro").click(function(){
    registrar();
});

/* levanta el modal para ingresar datos */
function registrar() {
    $("#titulo-modal-registro").html("Registro"); 
    document.getElementById("form-registro").reset();
	document.getElementById("form-rut").reset();

    $("#btn-aceptar-registro").attr("onClick", "agregarBD()");
    $("#modal-registro").modal("show");
}

function agregarBD() {
    val_usuario = validarFormularioRegistro('#usuario', "usuario");
    val_email = validarFormularioRegistro('#email', "correo");
    val_contrasena = validarFormularioRegistro('#contrasena', "contraseña");
    val_confirmar = validarFormularioRegistro('#confirmar', "confirmar");
    val_direccion = validarFormularioRegistro('#direccion', "direccion");

    if (val_usuario == false || val_email == false || val_contrasena == false || val_confirmar == false || val_direccion == false){
         return false;
    }
  
    var rut = $('#rut').val();
    var usuario = $('#usuario').val();
    var email = $('#email').val();
    var contrasena = $('#contrasena').val();
    var confirmar = $('#confirmar').val();
    var direccion = $('#direccion').val();

    if (contrasena == confirmar){
        $.ajax({
            dataType: 'json',
            async: true,
            url: '../modelo/acciones_registro.php?accion=1',
            data: {rut: rut, 
                    usuario: usuario, 
                    email: email, 
                    contrasena: contrasena, 
                    direccion: direccion,
                    id_ciudad: id_ciudad, 
                    confirmar: confirmar},

            success: function (response) {    
                console.log(response);
                if (response.salida_exitosa) {          
                    $("#modal-registro").modal("hide");
                    swal('Registrado', 'Su registro se ha realizado con éxito', 'success');
          
                } 

                else {
                    swal('Error', response.mensaje[2], 'error');
                }
            }, 

            error: function (e) {
                swal('Error', e.responseText, 'error');
            }
        });
            console.log("Valido");
    }

    else{
        swal('Error', 'Las contraseñas no coinciden', 'error');
    }
}

function obtener_id_ciudad(){
    $(document).on('change', '#ciudad', function(event) {
        nom_ciudad = $("#ciudad option:selected").text();
    
        if (nom_ciudad != "Seleccionar"){
            $.ajax({
                dataType: 'json',
                async: true,
                url: '../modelo/acciones_registro.php?accion=3',
                data: {nombre: nom_ciudad},

                success: function (response) {    
                    if (response.salida_exitosa) {
    			        $.each(response.datos,function(index,item) {
                            id_ciudad = item["id_ciudad"]; 
                        });
                    } 

                    else {
                        swal('Error', response.mensaje[2], 'error');
                    }
                }, 

                error: function (e) {
                    swal('Error', e.responseText, 'error');
                }
            });
        }
    });
}

/* valida que los datos obligatorios tengan algún valor */
function validarFormularioRegistro (id, nombre) {
  if ($(id).val().trim().length<1) {
    swal('Atención', "No ha completado el campo " + nombre ,  'info');
    return false;
  }
  
  return true;
}

