$(function() {  
    obtenerEspecialidades();
});

function obtenerEspecialidades() {
	var accion_agregar = "<button type='button' class='btn btn-dark' " +
                       "onclick='agregar();' title='Agregar'>" + 
                        "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-plus-lg' viewBox='0 0 16 16'>" +
						"<path d='M8 0a1 1 0 0 1 1 1v6h6a1 1 0 1 1 0 2H9v6a1 1 0 1 1-2 0V9H1a1 1 0 0 1 0-2h6V1a1 1 0 0 1 1-1z'/>" +
						"</svg></button>";
                       


	var tabla = $('#tabla_pago').dataTable({
		"columnDefs": [
		  {"title": "Nombre", "targets": 0, "orderable": true, "className": "dt-body-center"},
		  {"title": accion_agregar, "targets": 1, "orderable": false, "className": "dt-nowrap dt-right"},
		 
		],
		"searching": true,
		"search": {
		  "regex": true,
		  "smart": true
		},
		"scrollX": false,
		"order": [[1, "desc"]],
		"bDestroy": true,
		"deferRender": true,
		"language": {
		  "url": "../language/es.txt"
		},
		"pageLength": 10,
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": true,
		"bAutoWidth": false
	});

	tabla.fnClearTable();

	$.ajax({
	    url: '../modelo/acciones_pago.php',
	    data: {accion: 1},
	    type: 'POST',
	    dataType: 'json',
	    async: true,
	    success: function(response) {
		    if (response.salida_exitosa) {
			    var data = response.datos;
		
			    for(var i = 0; i < data.length; i++) {
				    tabla.fnAddData([
				        data[i]["nombre"],
				        "<button type='button' class='btn btn-warning btn-xs' onclick='editar(" + data[i]["id_pago"] + ");' title='Editar'>"+
				        "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='white' class='bi bi-pencil-square' viewBox='0 0 16 16'> " +
				        "<path d='M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 " + 
                        "0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z'/> <path fill-rule='evenodd' d='M1 " + 
                        "13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 " +
                        ".5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z'/></svg></button>&nbsp;" +
				
				        "<button type='button' class='btn btn-danger btn-xs' onclick='eliminar(" + data[i]["id_pago"] + ");' title='Eliminar'>"+
				        "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-trash' viewBox='0 0 16 16'> " +
				        "<path d='M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 " + 
                        ".5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z'/> "+
				        "<path fill-rule='evenodd' d='M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 " + 
                        "1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z'/></svg></button>"
			        ]);
		        }
		    }
 
            else {
			    swal('Error', response.mensaje, 'error');
		    }      
	    }, 
	
	    error: function(jqXHR, textStatus, errorThrown ) {
		    swal('Error', textStatus + " " + errorThrown, 'error');
	    }
	});
}

/* levanta el modal para ingresar datos */
function agregar() {
    $("#titulo-modal-especialidad").html("Agregar tipo de pago"); 
    document.getElementById("form-especialidad").reset();
    $("#btn-aceptar-especialidad").attr("onClick", "agregarBD()");
    $("#modal-especialidad").modal("show");
}

function agregarBD() {
    val = validarFormularioEspecialista();
    if (val == false) return false;
  
    var form = $('#nom_espec').val().trim();
	
    $.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_pago.php?accion=2',
        data: {nombre:form},
        success: function (response) {    
            if (response.salida_exitosa) {          
                $("#modal-especialidad").modal("hide");
                obtenerEspecialidades();
          
            } 
            else {
                swal('Error', response.mensaje, 'error');
            }
        },
 
        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    }); 
}

/* obtiene datos de una especialidad y los muestra en el modal */
function editar(id_pago) {
    $.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_pago.php?accion=4',
        data: {id_pago: id_pago},
        success: function (response) {    
            if (response.salida_exitosa) {
                $.each(response.datos, function(index, value) {
                    $("#nom_espec").val(value["nombre"]);
                });
    
                $("#titulo-modal-especialidad").html("Editar");
                $("#btn-aceptar-especialidad").attr("onClick", "editarBD(" + id_pago + ")");
                $("#modal-especialidad").modal("show");
            } 

            else {
		        swal('Error', response.mensaje, 'error');
            }   
        },
 
        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    }); 
}

/* actualiza los datos en la base de datos */
function editarBD(id_pago) {
    val = validarFormularioEspecialista();
  
    if (val == false) return false;
    var form = $('#nom_espec').val();

    $.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_pago.php?accion=5',
        data: {id_pago:id_pago, nombre:form},
        success: function (response) {    
            if (response.salida_exitosa) {          
                $("#modal-especialidad").modal("hide");
                obtenerEspecialidades();
            } 

            else {
                swal('Error', response.mensaje, 'error');
            }
        }, 

        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    }); 
}

/* elimina un registro de la base de datos */
function eliminar(id_pago) {
    swal({
        title:"¿Seguro que deseas eliminar?",
        text: "Una vez eliminado, no podrás recuperarlo",
        icon: "error",
        dangerMode: true,
        buttons: true,
    })
        
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                dataType: 'json',
                async: true,
                url: '../modelo/acciones_pago.php',
                data: {accion: 3, id_pago: id_pago},
                success: function (response) {    
                    if (response.salida_exitosa) {
                        swal({
                            title: "Éxito",
                            text: "¡Se ha eliminado exitósamente!",
                            icon: "success",
                        });
                    
                        obtenerEspecialidades();
                    }
                    
                    else {
                        swal('Error', response.mensaje, 'error');
                    }
                },

                error: function (e) {
                    swal('Error', e.responseText, 'error');
                }
            });
        }

        else {
            swal({
                title: "Cancelado",
                text: "¡La eliminación se ha cancelado!", 
                icon: "info",
            });
        }
    });
}

/* valida que los datos obligatorios tengan algún valor */
function validarFormularioEspecialista () {
    if ($('#nom_espec').val().trim().length<1) {
        swal('Atención', "El tipo de pago es requerido", 'info');
        return false;
    }
  
    return true;
}

