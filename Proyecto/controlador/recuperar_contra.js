$("#recuperar").click(function(){
    $("#titulo-modal-recuperar").html("Recuperar contraseña"); 
    document.getElementById("form-recuperar").reset();

    $("#btn-aceptar-recuperar").attr("onClick", "obtener_contra()");
    $("#modal-recuperar").modal("show");
});


function obtener_contra() {
    val_correo = validarFormularioRecuperar('#correo_recuperar', "correo");

    if (val_correo == false){
         return false;
    }
  
    var correo = $('#correo_recuperar').val();

    $.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_recuperar.php?accion=1',
        data: {correo: correo},  

        success: function (response) {    
            if (response.salida_exitosa) {          
                $("#modal-recuperar").modal("hide");
                swal('Éxito', 'Su contraseña es ' + response.datos, 'success');
          
            } 

            else {
                swal('Error', response.mensaje[2], 'error');
            }
        }, 

        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    });
}

/* valida que los datos obligatorios tengan algún valor */
function validarFormularioRecuperar(id, nombre) {
  if ($(id).val().length < 1) {
    swal('Atención', "No ha completado el campo " + nombre ,  'info');
    return false;
  }
  
  return true;
}
