$(function() { 

    obtener_productos("");   
    $("#btn_filtro").click(function () {
	    var input = $('#filtro').val();
        obtener_productos(input);   
    });
});

function obtener_productos(filtro) {
    var nombre = document.getElementById("btn_circulo").value;

	var sin_filtro = "<button type='button' class='btn btn-dark' " +
                        "onclick='obtener_productos(\"\");' title='Mostrar todo'>" +
                        "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='white' class='bi bi-arrow-repeat' " +
                            "viewBox='0 0 16 16'>" +
                            "<path d='M11.534 7h3.932a.25.25 0 0 1 .192.41l-1.966 2.36a.25.25 0 0 1-.384 0l-1.966-2.36a.25.25 0 0 " + 
                            "1 .192-.41zm-11 2h3.932a.25.25 0 0 0 .192-.41L2.692 6.23a.25.25 0 0 0-.384 0L.342 8.59A.25.25 0 0 0 .534 9z'/>" +
                            "<path fill-rule='evenodd' d='M8 3c-1.552 0-2.94.707-3.857 1.818a.5.5 0 1 1-.771-.636A6.002 6.002 0 0 1 13.917 " + 
                            "7H12.9A5.002 5.002 0 0 0 8 3zM3.1 9a5.002 5.002 0 0 0 8.757 2.182.5.5 0 1 1 .771.636A6.002 6.002 0 0 1 2.083 9H3.1z'/>"
                        "</svg>" + 
					"</button>";

	var tabla = $('#tabla_filtrado').dataTable({
		"columnDefs": [
		  {"title": " ", "targets": 0, "orderable": true, "className": "dt-body-center"},
		  {"title": sin_filtro, "targets": 1, "orderable": false, "className": "dt-nowrap dt-right"}
		],

		"searching": false,
		"search": {
		  "regex": true,
		  "smart": true
		},

		"scrollX": false,
		"order": [[1, "desc"]],
		"bDestroy": true,
		"deferRender": true,
		"language": {"url": "../language/es.txt"},
		"pageLength": 3,
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": false,
		"bAutoWidth": false
	});

	tabla.fnClearTable();

    $.ajax({
		url: '../modelo/acciones_pro.php',
		data: {accion: 1, 
                filtro: filtro,
                nombre: nombre},

		type: 'POST',
		dataType: 'json',
		async: true,

        success: function(response) {
            console.log(response);
			if (response.salida_exitosa) {
				var data = response.datos;
                var n = data.length;

                if(data.length > 1){
                    n = data.length - 1;
                }

				for(var i = 0; i <= n; i+=2) {
                    var carta1 = 
                        "<div class='card' align='center'>" + 
					        "<div class='text-center'>" +
						        "<img src='../vista/imagenes/" + data[i]["ima_producto"] + "\' " + " class='img-fluid yoimagen rounded-square' height='10%' width='22%'>" +
					        "</div>" +
					
					        "<div class='card-block'>" +
						        "<br>" + 
						        "<h4 class='card-title'>" + data[i]["nombre"] + "</h4>" +
						        "<p class='card-text'>" + data[i]["descripcion"] + "</p>" +
						        "<p class='card-text'> $ " + data[i]["precio"] + "</p>" +
						        "<h3 ' ' </h3>" + 
					        "</div>" +
					
					        "<div class='card-footer text-center'>" +
					            "<button type='button' class='btn btn-dark btn-xs'" +
                                     "onclick=\"info_carrito(" + 
                                        "\'" + data[i]["ima_producto"] + "\'," + 
                                        "\'" + data[i]["nombre"] + "\'," +
                                        "\'" + data[i]["descripcion"] + "\'," +
                                        data[i]["precio"] + ")\";" +
                                    "title='Comprar'> Comprar" +
                                "</button>"+
					        "</div>" +
				        "</div>";

                    var carta2 = "<p> </p>"
                    if ((data.length > 1) && (i+1 < data.length)){
                        carta2 = 
                            "<div class='card' align='center'>" + 
					            "<div class='text-center'>" +
						            "<img src='../vista/imagenes/" + data[i + 1]["ima_producto"] + "\' " + " class='img-fluid yoimagen rounded-square' height='300%' width='35%'>" +
					            "</div>" +
					
					            "<div class='card-block'>" +
						            "<br>" + 
						            "<h4 class='card-title'>" + data[i + 1]["nombre"] + "</h4>" +
						            "<p class='card-text'>" + data[i + 1]["descripcion"] + "</p>" +
						            "<p class='card-text'> $ " + data[i + 1]["precio"] + "</p>" +
						            "<h3 ' ' </h3>" + 
					            "</div>" +
					
					            "<div class='card-footer text-center'>" +
					                "<button type='button' class='btn btn-dark btn-xs'" +
                                        "onclick=\"info_carrito(" + 
                                            "\'" + data[i]["ima_producto"] + "\'," + 
                                            "\'" + data[i]["nombre"] + "\'," +
                                            "\'" + data[i]["descripcion"] + "\'," +
                                            data[i]["precio"] + ")\";" +
                                        "title='Comprar'> Comprar" +
                                    "</button>"+
					            "</div>" +
				            "</div>";
                    }

					tabla.fnAddData([
                        carta1, 
                        carta2
				    ]);
                    
                }   
			} 

            else {
				swal('Error', response.mensaje, 'error');
			}      
		},

        error: function(jqXHR, textStatus, errorThrown ) {
			swal('Error', textStatus + " " + errorThrown, 'error');
		}
    });

}

function info_carrito(imagen, nombre, descripcion, precio){
    var nombre_categoria = document.getElementById("btn_circulo").value;
    var imagen_categoria = document.getElementById("btn_cuadrado").value;

	$.ajax({
	    url: '../lib/carrito.php',
		data: {accion: 1,
                nombre: nombre,
                imagen: imagen,
                descripcion: descripcion,
                precio: precio,
                lugar: "categoria",
                nombre_categoria,
                imagen_categoria},

	    type: 'POST',
	    dataType: 'json',
	    async: true,

		success: function (response) {    
		    if (response.salida_exitosa) {          
                window.location = response.ubicacion;
                console.log(response);
		    }

            else {
			    swal('Error', response.mensaje, 'error');
		    }

		},
 
        error: function (e) {
		    swal('Error', e.responseText, 'error');
		}

    });
}
