$(function() { 
    var id_categoria;
    var id_oferta;
    var nom_archivo;

    obtener_productos();   
    categoria_seleccionada();
    oferta_seleccionada();
    definir_calendario();   
});

function definir_calendario(){
    $('#calendario').datepicker({
        format: "dd/mm/yyyy",
        weekStart: 1,
        language: "es",
        minDate: 0
    });
}

function obtener_productos() {
	var accion_agregar = "<button type='button' class='btn btn-dark' " +
                        "onclick='agregar_producto();' title='Agregar'>" + 
                        "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-plus-lg' viewBox='0 0 16 16'>" +
						"<path d='M8 0a1 1 0 0 1 1 1v6h6a1 1 0 1 1 0 2H9v6a1 1 0 1 1-2 0V9H1a1 1 0 0 1 0-2h6V1a1 1 0 0 1 1-1z'/>" +
						"</svg></button>&nbsp;" +

                        "<button type='button' class='btn btn-dark' " +
                        "onclick='obtener_ofertas();' title='Agregar oferta'>" + 
                        "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='white' class='bi bi-calendar-plus-fill' viewBox='0 0 16 16'>" +
                        "<path d='M4 .5a.5.5 0 0 0-1 0V1H2a2 2 0 0 0-2 2v1h16V3a2 2 0 0 0-2-2h-1V.5a.5.5 0 0 0-1 0V1H4V.5zM16 14V5H0v9a2 2 0 0 0 2 2h12a2 2 0 " +
                        "0 0 2-2zM8.5 8.5V10H10a.5.5 0 0 1 0 1H8.5v1.5a.5.5 0 0 1-1 0V11H6a.5.5 0 0 1 0-1h1.5V8.5a.5.5 0 0 1 1 0z'/>" +
                        "</svg></button>";     
       
	var tabla = $('#tabla_productos').dataTable({
		"columnDefs": [
		  {"title": "Nombre", "targets": 0, "orderable": true, "className": "dt-body-center"},
		  {"title": "Precio", "targets": 1, "orderable": true, "className": "dt-body-center"},
		  {"title": "Stock", "targets": 2, "orderable": true, "className": "dt-body-center"},
		  {"title": "Categoría", "targets": 3, "orderable": true, "className": "dt-body-center"},
		  {"title": "Descripción", "targets": 4, "orderable": true, "className": "dt-body-center"},
		  {"title": "Imagen", "targets": 5, "orderable": true, "className": "dt-body-center"},
		  {"title": "Oferta", "targets": 6, "orderable": true, "className": "dt-body-center"},
		  {"title": "Fecha fin", "targets": 7, "orderable": true, "className": "dt-body-center"},
		  {"title": accion_agregar, "targets": 8, "orderable": false, "className": "dt-nowrap dt-right"},
		 
		],

		"searching": true,
		"search": {
		  "regex": true,
		  "smart": true
		},

		"scrollX": false,
		"order": [[1, "desc"]],
		"bDestroy": true,
		"deferRender": true,
		"language": {"url": "../language/es.txt"},
		"pageLength": 10,
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": true,
		"bAutoWidth": false
	});

	tabla.fnClearTable();

    $.ajax({
		url: '../modelo/acciones_productos.php',
		data: {accion: 1},
		type: 'POST',
		dataType: 'json',
		async: true,

        success: function(response) {
			if (response.salida_exitosa) {
				var data = response.datos;

				for(var i = 0; i < data.length; i++) {
			        var oferta = "No";

                    if(data[i]["id_oferta"] != null){
                        oferta = "Si";
                    }                        
                    
					tabla.fnAddData([
					    data[i]["nombre"],
					    data[i]["precio"],
					    data[i]["stock"],
					    data[i]["categoria"],
					    data[i]["descripcion"],
					    data[i]["ima_producto"],
					    oferta,
					    data[i]["fecha"],

    					"<button type='button' class='btn btn-warning btn-xs' onclick='editar_producto(" + data[i]["id_producto"] + ");' title='Editar'>"+
	    				"<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='white' class='bi bi-pencil-square' viewBox='0 0 16 16'> " +
		    			"<path d='M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 " + 
                        "9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z'/> <path fill-rule='evenodd' " +
                        "d='M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 " +
					    "0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z'/></svg></button>&nbsp;" +

    					"<button type='button' class='btn btn-dark btn-xs' onclick='producto_oferta(" + data[i]["id_producto"] + ");' title='Producto en oferta'>"+
                        "<svg xmlns='http://www.w3.org/2000/svg' width='18' height='18' fill='white' class='bi bi-currency-dollar' viewBox='0 0 16 16'>" +
                        "<path d='M4 10.781c.148 1.667 1.513 2.85 3.591 3.003V15h1.043v-1.216c2.27-.179 3.678-1.438 3.678-3.3 0-1.59-.947-2.51-2." + 
                        "956-3.028l-.722-.187V3.467c1.122.11 1.879.714 2.07 1.616h1.47c-.166-1.6-1.54-2.748-3.54-2.875V1H7.591v1.233c-1.939.23-3.27 " +
                        "1.472-3.27 3.156 0 1.454.966 2.483 2.661 2.917l.61.162v4.031c-1.149-.17-1.94-.8-2.131-1.718H4zm3.391-3.836c-1.043-.263-1.6-.825-" +
                        "1.6-1.616 0-.944.704-1.641 1.8-1.828v3.495l-.2-.05zm1.591 1.872c1.287.323 1.852.859 1.852 1.769 0 1.097-.826 1.828-2.2 1.939V8.73l.348.086z'/></svg></button>&nbsp;" +


					    "<button type='button' class='btn btn-dark btn-xs' onclick=\"editar_imagen(" + "\'" + data[i]["ima_producto"] + "\'," + 
                        data[i]["id_producto"] + ")\"; title='Editar imagen'>"+
                        "<svg xmlns='http://www.w3.org/2000/svg' width='18' height='16' fill='currentColor' class='bi bi-image' viewBox='0 0 16 16'>" +
                        "<path d='M6.002 5.5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z'/>" +
                        "<path d='M2.002 1a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V3a2 2 0 0 0-2-2h-12zm12 1a1 1 0 0 1 1 1v6.5l-3.777-1.947a.5.5 " +
                        "0 0 0-.577.093l-3.71 3.71-2.66-1.772a.5.5 0 0 0-.63.062L1.002 12V3a1 1 0 0 1 1-1h12z'/></svg></button>&nbsp;" +


					    "<button type='button' class='btn btn-danger btn-xs' onclick=\"eliminar_producto(" + "\'" + data[i]["ima_producto"] + "\'," + 
                        data[i]["id_producto"] + ")\"; title='Eliminar'>"+
					    "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-trash' viewBox='0 0 16 16'> " +
					    "<path d='M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 " + 
                        ".5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z'/> "+
					    "<path fill-rule='evenodd' d='M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 " + 
                        "1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z'/></svg></button>"
				    ]);
                }   
			} 

            else {
				swal('Error', response.mensaje, 'error');
			}      
		},

        error: function(jqXHR, textStatus, errorThrown ) {
			swal('Error', textStatus + " " + errorThrown, 'error');
		}
    });
}

function obtener_ofertas(){
	var accion_agregar = "<button type='button' class='btn btn-dark' " +
                        "onclick='agregar_oferta();' title='Agregar'>" + 
                        "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-plus-lg' viewBox='0 0 16 16'>" +
						"<path d='M8 0a1 1 0 0 1 1 1v6h6a1 1 0 1 1 0 2H9v6a1 1 0 1 1-2 0V9H1a1 1 0 0 1 0-2h6V1a1 1 0 0 1 1-1z'/>" +
                        "</svg></button>";     

	var tabla = $('#tabla_ofertas').dataTable({
		"columnDefs": [
		  {"title": "Identificador", "targets": 0, "orderable": true, "className": "dt-body-center"},
		  {"title": "Fecha fin", "targets": 1, "orderable": true, "className": "dt-body-center"},
		  {"title": "Porcentaje", "targets": 2, "orderable": true, "className": "dt-body-center"},
		  {"title": accion_agregar, "targets": 3, "orderable": false, "className": "dt-nowrap dt-right"},
		],

		"searching": true,
		"search": {
		  "regex": true,
		  "smart": true
		},

		"scrollX": false,
		"order": [[1, "desc"]],
		"bDestroy": true,
		"deferRender": true,
		"language": {"url": "../language/es.txt"},
		"pageLength": 10,
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": true,
		"bAutoWidth": false
	});

	tabla.fnClearTable();
	$.ajax({
	    url: '../modelo/acciones_oferta.php',
	    data: {accion: 1},
	    type: 'POST',
	    dataType: 'json',
	    async: true,
	    success: function(response) {
		    if (response.salida_exitosa) {
			    var data = response.datos;
		
			    for(var i = 0; i < data.length; i++) {
                    identificador = "Oferta " + data[i]["id_oferta"];

				    tabla.fnAddData([
                        identificador,
				        data[i]["fecha_fin"],
				        data[i]["porcentaje"],

				        "<button type='button' class='btn btn-warning btn-xs' onclick='editar_oferta(" + data[i]["id_oferta"] + ");' title='Editar'>"+
				        "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='white' class='bi bi-pencil-square' viewBox='0 0 16 16'> " +
				        "<path d='M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 " + 
                        "0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z'/> <path fill-rule='evenodd' d='M1 " + 
                        "13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 " +
                        ".5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z'/></svg></button>&nbsp;" +
				
				        "<button type='button' class='btn btn-danger btn-xs' onclick='eliminar_oferta(" + data[i]["id_oferta"] + ");' title='Eliminar'>"+
				        "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-trash' viewBox='0 0 16 16'> " +
				        "<path d='M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 " + 
                        ".5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z'/> "+
				        "<path fill-rule='evenodd' d='M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 " + 
                        "1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z'/></svg></button>"
			        ]);
		        }
		    }
 
            else {
			    swal('Error', response.mensaje, 'error');
		    }      
	    }, 
	
	    error: function(jqXHR, textStatus, errorThrown ) {
		    swal('Error', textStatus + " " + errorThrown, 'error');
	    }
	});
    $("#modal-oferta").modal("show");
}

function agregar_oferta() {
    $("#titulo-modal-oferta2").html("Agregar oferta"); 
    document.getElementById("form-oferta2").reset();
    $("#btn-aceptar-oferta2").attr("onClick", "agregar_ofertaBD()");
    $("#modal-oferta2").modal("show");
}

function agregar_ofertaBD() {
    val_porcentaje = validarFormularioProducto('#porcentaje', 'porcentaje');
    val_fecha = validarFormularioProducto('#fecha', 'fecha de fin');
    if (val_porcentaje == false || val_fecha == false){ 
        return false;
    }
  
    var porcentaje = $('#porcentaje').val();
    var fecha = $('#fecha').val();
	
    $.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_oferta.php?accion=2',
        data: {porcentaje: porcentaje,
                fecha: fecha},

        success: function (response) {    
            if (response.salida_exitosa) {          
                $("#modal-oferta2").modal("hide");
                obtener_ofertas();
            } 

            else {
                swal('Error', response.mensaje, 'error');
            }
        },
 
        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    }); 
}

function eliminar_oferta(id_oferta) {
    swal({
        title:"¿Seguro que deseas eliminar?",
        text: "Una vez eliminado, no podrás recuperarlo",
        icon: "error",
        dangerMode: true,
        buttons: true,
    })
        
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                dataType: 'json',
                async: true,
                url: '../modelo/acciones_oferta.php',
                data: {accion: 3, 
                        id_oferta: id_oferta},
                success: function (response) {    
                    if (response.salida_exitosa) {
                        swal({
                            title: "Éxito",
                            text: "¡Se ha eliminado exitósamente!",
                            icon: "success",
                        });
                    
                        obtener_ofertas();
                    }
                    
                    else {
                        swal('Error', response.mensaje, 'error');
                    }
                },

                error: function (e) {
                    swal('Error', e.responseText, 'error');
                }
            });
        }

        else {
            swal({
                title: "Cancelado",
                text: "¡La eliminación se ha cancelado!", 
                icon: "info",
            });
        }
    });
}

function editar_oferta(id_oferta) {
    $.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_oferta.php?accion=4',
        data: {id_oferta: id_oferta},
        success: function (response) {    
            if (response.salida_exitosa) {
                console.log(response.datos);
                $.each(response.datos, function(index, value) {
                    $("#porcentaje").val(value["porcentaje"]);
                    $("#fecha").val(value["fecha_fin"]);
                });
    
                $("#titulo-modal-oferta2").html("Editar oferta");
                $("#btn-aceptar-oferta2").attr("onClick", "editar_ofertaBD(" + id_oferta + ")");
                $("#modal-oferta2").modal("show");
            } 

            else {
		        swal('Error', response.mensaje, 'error');
            }   
        },
 
        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    }); 
}

function editar_ofertaBD(id_oferta) {
    val_porcentaje = validarFormularioProducto('#porcentaje', 'porcentaje');
    val_fecha = validarFormularioProducto('#fecha', 'fecha de fin');
    if (val_porcentaje == false || val_fecha == false){ 
        return false;
    }
  
    var porcentaje = $('#porcentaje').val();
    var fecha = $('#fecha').val();

    $.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_oferta.php?accion=5',
        data: {id_oferta: id_oferta, 
                porcentaje: porcentaje,
                fecha: fecha},
        success: function (response) {    
            if (response.salida_exitosa) {          
                $("#modal-oferta2").modal("hide");
                obtener_ofertas();
            } 

            else {
                swal('Error', response.mensaje, 'error');
            }
        }, 

        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    }); 
}

function categoria_seleccionada(){
	$.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_productos.php',
        data: {accion: 6},
        success: function (response) {   
		    if (response.salida_exitosa) {
			    var items="";
				items ="<option> Seleccionar </option>";

			    $.each(response.datos,function(index,item) {
				    items+="<option>"+item["nombre"]+"</option>";
			        $("#categoria_select").html(items);
			    });
            }
     
            else {
                swal('Error', response.mensaje, 'error');
            }
	    }, 

        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    }); 
}

function obtener_id_categoria(){
    $(document).on('change', '#categoria_select', function(event) {
        nom_categoria = $("#categoria_select option:selected").text();
    
        if (nom_categoria != "Seleccionar"){
            $.ajax({
                dataType: 'json',
                async: true,
                url: '../modelo/acciones_productos.php?accion=9',
                data: {nombre: nom_categoria},

                success: function (response) {    
                    if (response.salida_exitosa) {
    			        $.each(response.datos,function(index,item) {
                            id_categoria = item["id_mascota"]; 
                            console.log(id_categoria);
                        });
                    } 

                    else {
                        swal('Error', response.mensaje, 'error');
                    }
                }, 

                error: function (e) {
                    swal('Error', e.responseText, 'error');
                }
            });
        }
    });
}

function agregar_producto() {
    $("#titulo-modal-producto").html("Agregar producto"); 
	$("#producto").show();
	$("#btn_producto").show();
	$("#form_imagen_producto").show();

    document.getElementById("form-producto").reset();
    $('#estado_producto').html('');

    categoria_seleccionada();
    id_categoria = obtener_id_categoria();
    subir_producto();

    $("#btn-aceptar-producto").attr("onClick", "agregar_productoBD()");
    $("#modal-producto").modal("show");
}

function agregar_productoBD() {
    val_nombre = validarFormularioProducto('#nom_producto', "nombre");
    val_descripcion = validarFormularioProducto('#descripcion', "descripción");
    val_precio = validarFormularioProducto('#precio', "precio");
    val_stock = validarFormularioProducto('#cantidad', "cantidad de productos");

	if (val_nombre == false || val_descripcion == false || val_precio == false || val_stock == false){
         return false;
    }

    var nombre = $('#nom_producto').val();
	var descripcion = $('#descripcion').val();
	var precio = $('#precio').val();
	var stock = $('#cantidad').val();
     
    $.ajax({
		dataType: 'json',
		async: true,
		url: '../modelo/acciones_productos.php?accion=2',
		data: {nombre: nombre,
                descripcion: descripcion,
                precio: precio,
                stock: stock,
                archivo: nom_archivo,
                id_categoria: id_categoria},

		success: function (response) {    
            console.log(response);
		    if (response.salida_exitosa) {          
			    $("#modal-producto").modal("hide");
			    obtener_productos();
		    }

            else {
			    swal('Error', response.mensaje, 'error');
		    }

		},
 
        error: function (e) {
		    swal('Error', e.responseText, 'error');
		}

    });
}

function subir_producto() {
    $("#form_imagen_producto").on('submit', function(e) {
        e.preventDefault();
        
        $.ajax({
            type: 'POST',
            url: '../modelo/acciones_subir_producto.php',
            data: new FormData(this),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('#btn_producto').attr("disabled","disabled");
                $('#form_imagen_producto').css("opacity",".5");
            },
            success: function(response) { 
                $('#estado_producto').html('');
                
                if(response.estado == "ok") {
                    nom_archivo = response.nom_archivo;
                    $('#form_imagen_producto')[0].reset();
                    $('#estado_producto').html('<p class="alert alert-success">'+response.mensaje + ': ' + response.nom_archivo + '</p>');
                }
                
                else{
                    $('#estado_producto').html('<p class="alert alert-danger">'+response.mensaje+'</p>');
                }
                
                $('#form_imagen_producto').css("opacity","");
                $("#btn_producto").removeAttr("disabled");
            }
        });
    });
}

function eliminar_producto(archivo, id_producto){    
    console.log(id_producto, archivo);
    swal({
        title:"¿Seguro que deseas eliminar?",
        text: "Una vez eliminado, no podrás recuperarlo",
        icon: "error",
        dangerMode: true,
        buttons: true,
    })
        
    .then((willDelete) => {
        if (willDelete) {
            $.ajax({
                dataType: 'json',
                async: true,
                url: '../modelo/acciones_productos.php',
                data: {accion: 3, id_producto: id_producto},
                success: function (response) {    
                    console.log(response);
                    if (response.salida_exitosa) {
                        // Borrar archivo de la carpeta        
                        borrar_imagen(archivo);
                        swal({
                            title: "Éxito",
                            text: "¡Se ha eliminado exitósamente!",
                            icon: "success",
                        });
                    
                        obtener_productos();
                    }
                    
                    else {
                        swal('Error', response.mensaje, 'error');
                    }
                },

                error: function (e) {
                    swal('Error', e.responseText, 'error');
                }
            });
        }

        else {
            swal({
                title: "Cancelado",
                text: "¡La eliminación se ha cancelado!", 
                icon: "info",
            });
        }
    });
}

function editar_imagen(archivo, id_producto){
    $("#titulo-modal-imagen").html("Editar imagen"); 
    $("#label_imagen").html("Imagen producto:"); 
    $('#estado_imagen').html('');

    nueva_imagen_producto();

    $("#btn-aceptar-imagen").attr("onClick", "editar_imagenBD(" + id_producto + "," + "\'" + archivo + "\')");
    $("#modal-imagen").modal("show");
}

function editar_imagenBD(id_producto, archivo){
    $.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_productos.php',
        data: {accion: 8, 
                id_producto: id_producto,
                archivo: nom_archivo},

        success: function (response) {    
            console.log(response);
            if (response.salida_exitosa) {
                // Borrar archivo de la carpeta        
                borrar_imagen(archivo);

                $("#modal-imagen").modal("hide");
                obtener_productos();
            }
                    
            else {
                swal('Error', response.mensaje, 'error');
            }
        },

        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    });
}

function editar_producto(id_producto) {
    categoria_seleccionada();
    id_categoria = obtener_id_categoria();

    console.log(id_categoria);
    $.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_productos.php?accion=4',
        data: {id_producto: id_producto},
        success: function (response) {    
            if (response.salida_exitosa) {
                $.each(response.datos, function(index, value) {
                    $("#nom_producto").val(value["nombre"]);
                    $("#descripcion").val(value["descripcion"]);
                    $("#precio").val(value["precio"]);
                    $("#cantidad").val(value["stock"]);
                    $("#categoria_select").val(value["categoria"]);
                });
    
                $("#titulo-modal-producto").html("Editar");
                $("#form_imagen_producto").hide();
                $("#btn-aceptar-producto").attr("onClick", "editar_productoBD(" + id_producto + ")");
                $("#modal-producto").modal("show");
            } 

            else {
		        swal('Error', response.mensaje, 'error');
            }   
        },
 
        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    }); 
}

/* actualiza los datos en la base de datos */
function editar_productoBD(id_producto) {
    val_nombre = validarFormularioProducto('#nom_producto', "nombre");
    val_descripcion = validarFormularioProducto('#descripcion', "descripción");
    val_precio = validarFormularioProducto('#precio', "precio");
    val_stock = validarFormularioProducto('#cantidad', "cantidad de productos");

	if (val_nombre == false || val_descripcion == false || val_precio == false || val_stock == false){
         return false;
    }

    var nombre = $('#nom_producto').val();
	var descripcion = $('#descripcion').val();
	var precio = $('#precio').val();
	var stock = $('#cantidad').val();

    $.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_productos.php?accion=5',
		data: {nombre: nombre,
                descripcion: descripcion,
                precio: precio,
                stock: stock,
                id_categoria: id_categoria,
                id_producto: id_producto},

        success: function (response) {    
            if (response.salida_exitosa) {          
                console.log(response.datos);
                $("#modal-producto").modal("hide");
                obtener_productos();
            } 

            else {
                swal('Error', response.mensaje, 'error');
            }
        }, 

        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    }); 
}

function producto_oferta(id_producto){
    $("#titulo-modal-producto_oferta").html("Seleccionar oferta"); 

    $(document).on('change', '#oferta_select', function(event) {
        nom_categoria = $("#oferta_select option:selected").text();
 
       if (nom_categoria != "Seleccionar"){
            nom_categoria = nom_categoria.substring(8,9);
            id_oferta = parseInt(nom_categoria, 10); 
        }
    });

    $("#btn-aceptar-producto_oferta").attr("onClick", "producto_ofertaBD(" + id_producto + ")");
    $("#modal-producto_oferta").modal("show");
}

function producto_ofertaBD(id_producto){
    $.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_productos.php?accion=11',
		data: {id_oferta: id_oferta,
                id_producto: id_producto},

        success: function (response) {    
            if (response.salida_exitosa) {          
                console.log(response.datos);
                $("#modal-producto_oferta").modal("hide");
                obtener_productos();
            } 

            else {
                swal('Error', response.mensaje, 'error');
            }
        }, 

        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    }); 
}

function oferta_seleccionada(){
	$.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_productos.php',
        data: {accion: 10},
        success: function (response) {   
		    if (response.salida_exitosa) {
			    var items="";
				items ="<option> Seleccionar </option>";

			    $.each(response.datos,function(index,item) {
				    items+="<option> Oferta " + item["id_oferta"]+"</option>";
			        $("#oferta_select").html(items);
			    });
            }
     
            else {
                swal('Error', response.mensaje, 'error');
            }
	    }, 

        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    }); 
}

function obtener_id_oferta(){
}
function nueva_imagen_producto() {
    $("#form_imagen").on('submit', function(e) {
        e.preventDefault();
        
        $.ajax({
            type: 'POST',
            url: '../modelo/acciones_editar_sucursal.php',
            data: new FormData(this),
            dataType: 'json',
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function(){
                $('#btn_imagen').attr("disabled","disabled");
                $('#form_imagen').css("opacity",".5");
            },
            success: function(response) { 
                $('#estado_imagen').html('');
                
                if(response.estado == "ok") {
                    nom_archivo = response.nom_archivo;
                    $('#form_imagen')[0].reset();
                    $('#estado_imagen').html('<p class="alert alert-success">'+response.mensaje + ': ' + response.nom_archivo + '</p>');
                }
                
                else{
                    $('#estado_imagen').html('<p class="alert alert-danger">'+response.mensaje+'</p>');
                }
                
                $('#form_imagen').css("opacity","");
                $("#btn_imagen").removeAttr("disabled");
            }
        });
    });
}

function borrar_imagen(archivo) {
    $.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_productos.php?accion=7',
        data: {archivo: archivo},
        success: function (response) {    
            console.log(response);
        },

        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    });
}

/* valida que los datos obligatorios tengan algún valor */
function validarFormularioProducto (id, nombre) {
    if ($(id).val().length < 1) {
	    swal('Atención',"No ha completado el campo " + nombre, 'info');
		return false;
    }
	  
    return true;
}
