$(function() {  
    var id_ciudad;
    var id_perfil;

    obtener_usuarios();
    ciudades_registro();
    obtener_perfiles();
});

function obtener_usuarios() {
	var accion_agregar = "<button type='button' class='btn btn-dark' " +
                       "onclick='agregar_usuario();' title='Agregar'>" + 
                        "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-plus-lg' viewBox='0 0 16 16'>" +
						"<path d='M8 0a1 1 0 0 1 1 1v6h6a1 1 0 1 1 0 2H9v6a1 1 0 1 1-2 0V9H1a1 1 0 0 1 0-2h6V1a1 1 0 0 1 1-1z'/>" +
						"</svg></button>";
                       


	var tabla = $('#tabla_usuario').dataTable({
		"columnDefs": [
		  {"title": "Usuario", "targets": 0, "orderable": false, "className": "dt-body-center", "visible": true},
		  {"title": "Rut", "targets": 1, "orderable": true, "className": "dt-body-center"},
		  {"title": "Correo", "targets": 2, "orderable": true, "className": "dt-body-center"},
		  {"title": "Dirreción", "targets": 3, "orderable": true, "className": "dt-body-center"},
		  {"title": "Ciudad", "targets": 4, "orderable": true, "className": "dt-body-center"},
		  {"title": "Perfil", "targets": 5, "orderable": true, "className": "dt-body-center"},
		  {"title": accion_agregar, "targets": 6, "orderable": false, "className": "dt-nowrap dt-right"},
		 
		],
		"searching": true,
		"search": {
		  "regex": true,
		  "smart": true
		},
		"scrollX": false,
		"order": [[1, "desc"]],
		"bDestroy": true,
		"deferRender": true,
		"language": {
		  "url": "../language/es.txt"
		},
		"pageLength": 10,
		"bPaginate": true,
		"bLengthChange": true,
		"bFilter": true,
		"bInfo": true,
		"bAutoWidth": false
	});

	tabla.fnClearTable();

	$.ajax({
	    url: '../modelo/acciones_usuario.php',
	    data: {accion: 1},
	    type: 'POST',
	    dataType: 'json',
	    async: true,
	    success: function(response) {
		    if (response.salida_exitosa) {
			    var data = response.datos;
		
			    for(var i = 0; i < data.length; i++) {
				    tabla.fnAddData([
				        data[i]["usuario"],
				        data[i]["rut"],
				        data[i]["correo"],
				        data[i]["direccion"],
				        data[i]["ciudad"],
				        data[i]["perfil"],
				        
				        "<button type='button' class='btn btn-warning btn-xs' onclick=\"editar_usuario('" + data[i]["correo"] + "')\";' title='Editar'>"+
				        "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='white' class='bi bi-pencil-square' viewBox='0 0 16 16'> " +
				        "<path d='M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 " + 
                        "0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z'/> <path fill-rule='evenodd' d='M1 " + 
                        "13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 " +
                        ".5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z'/></svg></button>&nbsp;" +
				
				        "<button type='button' class='btn btn-danger btn-xs' onclick=\"eliminar_usuario('" + data[i]["correo"] + "')\";' title='Eliminar'>"+
				        "<svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-trash' viewBox='0 0 16 16'> " +
				        "<path d='M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 " + 
                        ".5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z'/> "+
				        "<path fill-rule='evenodd' d='M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 " + 
                        "1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z'/></svg></button>"
			        ]);
		        }
		    }
 
            else {
			    swal('Error', response.mensaje[2], 'error');
		    }      
	    }, 
	
	    error: function(jqXHR, textStatus, errorThrown ) {
		    swal('Error', textStatus + " " + errorThrown, 'error');
	    }
	}); 
}

/* levanta el modal para ingresar datos */
function agregar_usuario() {
	$("#titulo-modal-registro").html("Registro"); 
	document.getElementById("form-registro").reset();
	document.getElementById("form-rut").reset();

    ciudades_registro();
    obtener_perfiles();

    id_ciudad = id_ciudad_registro();
    id_perfil = obtener_id_perfiles();

    $("#btn-aceptar-registro").attr("onClick", "agregar_usuarioBD()");
    $("#modal-registro").modal("show");
}

function ciudades_registro() {
	$.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_registro.php',
        data: {accion:2},
        success: function (response) {   
		    if (response.salida_exitosa) {
			    var items="";
				items ="<option>Seleccionar</option>";

			    $.each(response.datos,function(index,item) {
				    items+="<option>" + item["nombre"]+"</option>";
			        $("#ciudad_registro").html(items);
			    });
            }
     
            else {
                swal('Error', response.mensaje[2], 'error');
            }
	
	    }, 

        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    }); 
} 

function obtener_perfiles() {
	$.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_usuario.php',
        data: {accion:6},
        success: function (response) {  
		    if (response.salida_exitosa) {
			    var items="";
				items ="<option>Seleccionar</option>";

			    $.each(response.datos,function(index,item) {
				    items+="<option>" + item["nombre"]+"</option>";
			        $("#perfil").html(items);
			    });
            }
     
            else {
                swal('Error', response.mensaje[2], 'error');
            }
	
	    }, 

        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    }); 
} 

function agregar_usuarioBD() {
    val_usuario = validarFormularioRegistro('#usuario', "usuario");
    val_email = validarFormularioRegistro('#email', "correo");
    val_contrasena = validarFormularioRegistro('#contrasena', "contraseña");
    val_confirmar = validarFormularioRegistro('#confirmar', "confirmar");
    val_direccion = validarFormularioRegistro('#direccion', "direccion");

    if (val_usuario == false || val_email == false || val_contrasena == false || val_confirmar == false || val_direccion == false){
         return false;
    }
  
    var rut = $('#rut').val();
    var usuario = $('#usuario').val();
    var email = $('#email').val();
    var contrasena = $('#contrasena').val();
    var confirmar = $('#confirmar').val();
    var direccion = $('#direccion').val();

    if (contrasena == confirmar){
        $.ajax({
            dataType: 'json',
            async: true,
            url: '../modelo/acciones_usuario.php?accion=2',
            data: {rut: rut, 
                    usuario: usuario, 
                    email: email, 
                    contrasena: contrasena, 
                    direccion: direccion,
                    id_ciudad: id_ciudad, 
                    id_perfil: id_perfil, 
                    confirmar: confirmar},

            success: function (response) {    
                if (response.salida_exitosa) {          
                    $("#modal-registro").modal("hide");
                    swal('Registrado', 'Su registro se ha realizado con éxito', 'success');
					obtener_usuarios();
                } 

                else {
                    swal('Error', response.mensaje[2], 'error');
                }
            }, 

            error: function (e) {
                swal('Error', e.responseText, 'error');
            }
        });
            console.log("Valido");
    }

    else{
        swal('Error', 'Las contraseñas no coinciden', 'error');
    }
}

function id_ciudad_registro(){
    $(document).on('change', '#ciudad_registro', function(event) {
        nom_ciudad = $("#ciudad_registro option:selected").text();
    
        if (nom_ciudad != "Seleccionar"){
            $.ajax({
                dataType: 'json',
                async: true,
                url: '../modelo/acciones_registro.php?accion=3',
                data: {nombre: nom_ciudad},

                success: function (response) {    
                    if (response.salida_exitosa) {
    			        $.each(response.datos,function(index,item) {
                            id_ciudad = item["id_ciudad"]; 
                        });
                    } 

                    else {
                        swal('Error', response.mensaje[2], 'error');
                    }
                }, 

                error: function (e) {
                    swal('Error', e.responseText, 'error');
                }
            });
        }
    });
}

function obtener_id_perfiles(){
    $(document).on('change', '#perfil', function(event) {
        nom_perfil = $("#perfil option:selected").text();
    
        if (nom_perfil != "Seleccionar"){
            $.ajax({
                dataType: 'json',
                async: true,
                url: '../modelo/acciones_usuario.php?accion=7',
                data: {nombre: nom_perfil},

                success: function (response) {    
                    if (response.salida_exitosa) {
    			        $.each(response.datos,function(index,item) {
                            id_perfil = item["id_perfil"]; 
                        });
                    } 

                    else {
                        swal('Error', response.mensaje[2], 'error');
                    }
                }, 

                error: function (e) {
                    swal('Error', e.responseText, 'error');
                }
            });
        }
    });
}

function editar_usuario(correo) {
    ciudades_registro();
    obtener_id_perfiles();
    id_ciudad_registro();

    $.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_usuario.php?accion=4',
        data: {correo: correo},
        success: function (response) {    
            if (response.salida_exitosa) {
                $.each(response.datos, function(index, value) {
                    $("#usuario").val(value["usuario"]);
                    $('#rut').val(value["rut"]);
                    $('#email').val(value["correo"]);
                    $('#direccion').val(value["direccion"]);
                    $('#ciudad_registro').val(value["ciudad"]);
                    $('#perfil').val(value["perfil"]);
                });
    
                
                $('#contrasena').val(response.contra);
                $('#confirmar').val(response.contra);


                $("#titulo-modal-registro").html("Editar");
                $("#btn-aceptar-registro").attr('onclick', "editar_usuarioBD(\'" + correo + "\')");
                $("#modal-registro").modal("show");
            } 

            else {
		        swal('Error', response.mensaje[2], 'error');
            }   
        },
 
        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    }); 
}

function editar_usuarioBD(correo) {
	console.log(correo);
    console.log(id_ciudad);
    console.log(id_perfil);
	
    val_usuario = validarFormularioRegistro('#usuario', "usuario");
    val_email = validarFormularioRegistro('#email', "correo");
    val_contrasena = validarFormularioRegistro('#contrasena', "contraseña");
    val_confirmar = validarFormularioRegistro('#confirmar', "confirmar");
    val_direccion = validarFormularioRegistro('#direccion', "direccion");

    if (val_usuario == false || val_email == false || val_contrasena == false || val_confirmar == false || val_direccion == false){
         return false;
    }
  
    var rut = $('#rut').val();
    var usuario = $('#usuario').val();
    var email = $('#email').val();
    var contrasena = $('#contrasena').val();
    var confirmar = $('#confirmar').val();
    var direccion = $('#direccion').val();

    $.ajax({
        dataType: 'json',
        async: true,
        url: '../modelo/acciones_usuario.php?accion=5',
		data: {rut: rut, 
                usuario: usuario, 
                nuevo: email, 
                antiguo: correo,
                contrasena: contrasena, 
                direccion: direccion,
                id_ciudad: id_ciudad, 
                id_perfil: id_perfil}, 
                    
        success: function (response) {    
            console.log(response);
            if (response.salida_exitosa) {          
                $("#modal-registro").modal("hide");
                obtener_usuarios();
            } 

            else {
                swal('Error', response.mensaje[2], 'error');
            }
        }, 

        error: function (e) {
            swal('Error', e.responseText, 'error');
        }
    });
}

function eliminar_usuario(correo){   
    swal({
        title:"¿Seguro que deseas eliminar?",
        text: "Una vez eliminado, no podrás recuperarlo",
        icon: "error",
        dangerMode: true,
        buttons: true,
    })
        
    .then((willDelete) => {
        if (willDelete) { 
		
            $.ajax({
                dataType: 'json',
                async: true,
                url: '../modelo/acciones_usuario.php',
                data: {accion: 3, correo: correo},
                success: function (response) {    
                  
                    if (response.salida_exitosa) {
                        swal({
                            title: "Éxito",
                            text: "¡Se ha eliminado exitósamente!",
                            icon: "success",
                        });
                    
                        obtener_usuarios();
                    }
                    
                    else {
                        swal('Error', response.mensaje[2], 'error');
                    }
                },

                error: function (e) {
                    swal('Error', e.responseText, 'error');
                }
            });
        }

        else {
            swal({
                title: "Cancelado",
                text: "¡La eliminación se ha cancelado!", 
                icon: "info",
            });
        }
    });

}

function validarFormularioRegistro (id, nombre) {
  if ($(id).val().trim().length<1) {
    swal('Atención', "No ha completado el campo" + nombre ,  'info');
    return false;
  }
  
  return true;
}

